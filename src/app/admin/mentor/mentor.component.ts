import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import * as $ from 'jquery';
import { AuthService } from 'src/app/services/authService';
import { Subject } from 'rxjs';
import { professorProfiles } from 'src/app/models/professorProfiles';
import 'datatables.net';

@Component({
  selector: 'app-mentor',
  templateUrl: './mentor.component.html',
  styleUrls: ['./mentor.component.css'],
})
export class MentorComponent implements OnInit {
  mentors: any = [];
  errorMessage: string;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(public authService: AuthService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.authService.selectProfessorsProfiles().subscribe((data: any[]) => {
      this.mentors = data["rows"];
      console.log(this.mentors);
      this.mentors.forEach(element => {
        //element['isEdit'] = false;
      })
      this.dtTrigger.next();
    });
  }

  getMentors() {
    this.authService.selectProfessorsProfiles() .subscribe((data: any[]) => {
      this.mentors = data["rows"];
      console.log(this.mentors);
    });
  }
  deleterow(){

  }

  approve(mentor:professorProfiles){
      console.log('ok')
  }
}
