import { Component, Injectable, OnDestroy, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { category } from 'src/app/models/category';
import { EducationService } from 'src/app/services/EducationService';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import 'datatables.net';
import { element } from 'protractor';
import { matiere } from 'src/app/models/matiere';
import { TranslateService } from '@ngx-translate/core';
import { SharedSendService } from 'src/app/services/SharedSendService';
@Injectable({
  providedIn: 'root'
})


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class categoryComponent implements OnInit {
  //datatable: any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  categorys = [];

  cat: category;
  categoryForm: FormGroup;
  public webUser: boolean = false;
  error: any;
  dataRegister: any = {}
  matiers: any;

  selectedMatier
  CategoryNameEnglish
  CategoryNameArabic
  CategoryDescriptionEnglish
  CategoryDescriptionArabic
  _categorys: any;
  clickEventSubscription: any;

  constructor(private formBuilder: FormBuilder,
    private educationService: EducationService,
    private toastr: ToastrService,  public translate: TranslateService,
    public sharedSendService: SharedSendService,
    public router: Router,) {
      translate.use(localStorage.getItem("currentLanguage"));
      this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {
        if (value === 'ar') {  this.translate.use('ar'); }
        if (value === 'en') {   this.translate.use('en'); }
        if (value === 'fr') {   this.translate.use('fr'); }
        if (value === 'Reload') { this.ngOnInit();}
      });

  }


  ngOnInit(): void {

    this.listMatiers();





    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
  }
  listgCategory() {
    this.educationService.getCategory().subscribe((data) => {
      this.categorys = data["rows"];

      console.log('categorys', this.categorys)
      this.categorys.forEach(element => {
        element['isEdit'] = false;
        element.matier = this.getNameOfMatierId(element.matiereId)


        console.log("******" + element.matiereId + '  ' + this.getNameOfMatierId(element.matiereId));
      })
      this.dtTrigger.next();
    });
  }
  listMatiers() {
    this.educationService.getMatiers().subscribe((data: any) => {
      this.matiers = data["rows"];
      console.log('matiers', this.matiers)
      this.listgCategory()
      // this.matiers.forEach(element => {

      //  });
    });
  }

  getNameOfMatierId(id) {
    var rslt = null;
    this.matiers.forEach(element => {
      if (element.id == id) {
        rslt = element
      }

    });

    return rslt
  }



  getCategories() {
    this.educationService.getCategory().subscribe((data: category[]) => {
      this.categorys = data["rows"];
      this.categorys.forEach(element => {

      });

    });
  }
  save() {
    this.cat = new category(
      1,
      this.selectedMatier,
      this.CategoryNameEnglish,
      this.CategoryNameArabic,
      this.CategoryDescriptionEnglish,
      this.CategoryDescriptionArabic,
      localStorage.getItem('token')
    );

    //console.log("--------->"+this.cat.token);
    this.educationService.addCategory(this.cat).subscribe(
      data => {
        this.dataRegister = data;
        if (this.dataRegister.success) {
          this.toastr.success('', 'category added with success');
          this.ngOnInit();

        }
        else {
          console.log(data);
          this.toastr.error(this.dataRegister.message, 'Add failed!');
        }

      },
      error => {

        this.toastr.error(error, 'Add failed!');
        this.error = error;
        console.log(error);
      }
    );

  }


  deleterow(category) {
    // console.log("DELETE ---->" + category);
    if (confirm("Are you sure to delete  : " + category.englishName)) {
      category.token = localStorage.getItem('token');
      this.educationService.deleteCategory(category).subscribe(
        data => {
          this.dataRegister = data;
          if (this.dataRegister.success) {
            this.toastr.success('', 'category deleted with success');

            this.ngOnInit();
            // this.reloadComponent();
          }
          else {
            //   console.log(data);
            this.toastr.error(this.dataRegister.message, 'deleted failed!');
          }

        },
        error => {

          this.toastr.error(error, 'Delete failed!');
          this.error = error;
          console.log(error);
        }
      );



    }

    //here do delete event


  }
  updaterow(category) {

    category.isEdit = true;
  }
  reloadComponent() {
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/admin/category']);
    });
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  saveChanges(category) {
    category.token = localStorage.getItem('token');
    // console.log("token------"+category.token);
    this.educationService.changeCategory(category).subscribe(
      data => {
        this.dataRegister = data;

        if (this.dataRegister.success) {
          this.toastr.success('', 'category updated with success');

          this.ngOnInit();
          // this.reloadComponent();
        }
        else {
          console.log(data);
          this.toastr.error(this.dataRegister.message, 'update failed!');
        }

      },
      error => {

        this.toastr.error(error, 'Update failed!');
        this.error = error;
        console.log(error);
      }
    );
    category.isEdit = false;
  }

}
