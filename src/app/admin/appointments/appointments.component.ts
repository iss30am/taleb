import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import * as $ from 'jquery';
import 'datatables.net';
import { AuthService } from 'src/app/services/authService';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css'],
})
export class AppointmentsComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  books :[];
  constructor(public authService: AuthService) { }

  ngOnInit(): void {


    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.authService.selectbooks() .subscribe((data:any[]) => {
      this.books = data["rows"];

      this.dtTrigger.next();
    });
  }


}
