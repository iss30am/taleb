import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import 'datatables.net';
import { element } from 'protractor';
import { Subject } from 'rxjs';
import { AuthService } from 'src/app/services/authService';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  users: any;
  error: any;
  constructor(private authService : AuthService  , private toastr: ToastrService,) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.authService.selectUsers().subscribe((data: any[]) => {
      this.users = data["rows"];
      this.users.forEach(element => {
        //element['isEdit'] = false;
      })
      this.dtTrigger.next();
    });

  }
active (user : any){
  if (user.active == 0){user.active =1} else {user.active = 0}
  console.log(user.active);
  user.token = localStorage.getItem('token');
  this.authService.blockUser(user).subscribe(
    data => {
      if (data.success) {

        this.toastr.success('', 'user updated');
       // this.ngOnInit();
      }
      else {
        this.toastr.error(data.message, 'update failed!');
      }
    },
    error => {
      this.toastr.error(error, 'update  failed!');
      this.error = error;
      // console.log(error);
    });
}
Approuved(user : any){

}
}
