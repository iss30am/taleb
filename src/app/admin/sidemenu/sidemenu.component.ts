import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Event, Router, NavigationStart } from '@angular/router';

import { CommonServiceService } from '../../common-service.service';
import { AppComponent } from 'src/app/app.component';
import { TranslateService } from '@ngx-translate/core';
import { SharedSendService } from 'src/app/services/SharedSendService';
@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css'],
})
export class SidemenuComponent implements OnInit {
  showDropdown = true;
  public bellCollapsed = true;
  public userCollapsed = true;
  splitVal;
  base = '';
  page = '';
  clickEventSubscription: any;

  constructor(
    @Inject(DOCUMENT) private document,
    public router: Router,
    private commonService: CommonServiceService,
    public translate: TranslateService,
    public sharedSendService: SharedSendService
  ) {
    translate.use(localStorage.getItem("currentLanguage"));
    this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {
      if (value === 'ar') {  this.translate.use('ar'); }
      if (value === 'en') {   this.translate.use('en'); }
      if (value === 'fr') {   this.translate.use('fr'); }
      if (value === 'Reload') { this.ngOnInit();}
    });




    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.splitVal = event.url.split('/');
        this.base = this.splitVal[1];
        this.page = this.splitVal[2];
      }
    });
  }
  ngOnInit(): void { }

  ngAfterViewInit() {
    this.loadDynmicallyScript('./../../../assets/admin/js/script.js');
  }
  loadDynmicallyScript(js) {
    var script = document.createElement('script');
    script.src = js;
    script.async = false;
    document.head.appendChild(script);
    script.onload = () => this.doSomethingWhenScriptIsLoaded();
  }

  doSomethingWhenScriptIsLoaded() { }
  change(name) {
    this.page = name;
    this.commonService.nextmessage('admin');
  }
  home() {
    // this.router.navigate(['/index']);
    window.location.href = '/index';
  }

  main() {
    this.commonService.nextmessage('main');
  }
  clickLogout() {
    localStorage.setItem('currentUser', null);
    console.log('clickLogout side bar', 'null');
    this.router.navigate(['/']);
    localStorage.setItem('currentUser', null);
    localStorage.setItem('token', null);
    localStorage.setItem('userId', null);
    AppComponent.auth = false;
    AppComponent.Uname = null;
  }
  bell() {
    this.bellCollapsed = !this.bellCollapsed;
    if (!this.userCollapsed) {
      this.userCollapsed = true;
    }
  }
  user() {
    this.userCollapsed = !this.userCollapsed;
    if (!this.bellCollapsed) {
      this.bellCollapsed = true;
    }
  }
}
