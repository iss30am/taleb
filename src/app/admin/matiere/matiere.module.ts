import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatiereRoutingModule } from './matiere-routing.module';
import { MatiereComponent } from './matiere.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { createTranslateLoader } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [MatiereComponent],
  imports: [
    CommonModule,
    MatiereRoutingModule,
    ReactiveFormsModule,
    DataTablesModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
  ],
})
export class MatiereModule { }
