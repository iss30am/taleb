import { Component, Injectable, OnDestroy, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { matiere } from 'src/app/models/matiere';
import { EducationService } from 'src/app/services/EducationService';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import 'datatables.net';
import { element } from 'protractor';
import { TranslateService } from '@ngx-translate/core';
import { SharedSendService } from 'src/app/services/SharedSendService';
@Injectable({
  providedIn: 'root'
})


@Component({
  selector: 'app-matiere',
  templateUrl: './matiere.component.html',
  styleUrls: ['./matiere.component.css']
})
export class MatiereComponent implements OnInit, OnDestroy {
  //datatable: any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  matiers: matiere[] = [];
  mat: matiere;
  matiereForm: FormGroup;
  public webUser: boolean = false;
  error: any;
  dataRegister: any = {}
  clickEventSubscription: any;

  constructor(private formBuilder: FormBuilder,
    private educationService: EducationService,
    private toastr: ToastrService,
    public router: Router,  public translate: TranslateService,
    public sharedSendService: SharedSendService) {
      translate.use(localStorage.getItem("currentLanguage"));
      this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {
        if (value === 'ar') {  this.translate.use('ar'); }
        if (value === 'en') {   this.translate.use('en'); }
        if (value === 'fr') {   this.translate.use('fr'); }
        if (value === 'Reload') { this.ngOnInit();}
      });


  }

  ngOnInit(): void {


    this.initForm();

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.educationService.getMatiers().subscribe((data: matiere[]) => {
      this.matiers = data["rows"];
      this.matiers.forEach(element => {
        element['isEdit'] = false;
      })
      this.dtTrigger.next();
    });

  }


  getMatiers() {

    this.educationService.getMatiers().subscribe((data: matiere[]) => {
      this.matiers = data["rows"];

    });

  }

  onSubmitForm() {

    const formValue = this.matiereForm.value;
    const newMatiere = new matiere(
      1,
      formValue['englishName'],
      formValue['englishDescription'],
      formValue['arabicName'],
      formValue['arabicDescription'],
      localStorage.getItem('token')
    );

    this.educationService.addMatiere(newMatiere).subscribe(
      data => {
        this.dataRegister = data;
        if (this.dataRegister.success) {
          this.toastr.success('', 'Matiere added with success');
          this.ngOnInit();
          //this.reloadComponent();
        }
        else {

          this.toastr.error(this.dataRegister.message, 'Add failed!');
        }

      },
      error => {

        this.toastr.error(error, 'Add failed!');
        this.error = error;
        // console.log(error);
      }
    );

  }
  initForm() {
    this.matiereForm = this.formBuilder.group({
      id: 1,
      englishName: ['', [Validators.required, , Validators.min(2)]],
      englishDescription: ['', [Validators.required, , Validators.min(2)]],
      arabicName: ['', [Validators.required, , Validators.min(2)]],
      arabicDescription: ['', [Validators.required, , Validators.min(2)]],
    });
  }

  deleterow(matier) {
    //console.log("DELETE ---->" + matier);
    if (confirm("Are you sure to delete  : " + matier.englishName)) {
      matier.token = localStorage.getItem('token');
      this.educationService.deleteMatier(matier).subscribe(
        data => {
          this.dataRegister = data;
          if (this.dataRegister.success) {
            this.toastr.success('', 'Matiere deleted with success');

            this.ngOnInit();
            // this.reloadComponent();
          }
          else {
            // console.log(data);
            this.toastr.error(this.dataRegister.message, 'deleted failed!');
          }

        },
        error => {

          this.toastr.error(error, 'Delete failed!');
          this.error = error;
          // console.log(error);
        }
      );



    }

    //here do delete event


  }
  updaterow(matier) {

    matier.isEdit = true;
  }
  reloadComponent() {
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/admin/matiere']);
    });
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  saveChanges(matier) {
    matier.token = localStorage.getItem('token');
    //console.log("token------"+matier.token);
    this.educationService.changeMatiers(matier).subscribe(
      data => {
        this.dataRegister = data;

        if (this.dataRegister.success) {
          this.toastr.success('', 'Matiere updated with success');

          this.ngOnInit();
          // this.reloadComponent();
        }
        else {
          //   console.log(data);
          this.toastr.error(this.dataRegister.message, 'update failed!');
        }

      },
      error => {

        this.toastr.error(error, 'Update failed!');
        this.error = error;
        console.log(error);
      }
    );
    matier.isEdit = false;
  }

}
