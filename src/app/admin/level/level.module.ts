import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LevelRoutingModule } from './level-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { LevelComponent } from './level.component';


@NgModule({
  declarations: [LevelComponent],
  imports: [
    CommonModule,
    LevelRoutingModule,
    ReactiveFormsModule,
    DataTablesModule,
    FormsModule
  ],
})
export class LevelModule { }
