import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-mentee',
  templateUrl: './mentee.component.html',
  styleUrls: ['./mentee.component.css'],
})
export class MenteeComponent implements OnInit {
  patientsList: any = [];
  errorMessage: string;

  constructor(public commonService: CommonServiceService) { }

  ngOnInit(): void {
    this.getMentees();
  }

  getMentees() {
    this.commonService.getpatients().subscribe(
      (res) => {
        this.patientsList = res;
        $(function () {
          $('table').DataTable();
        });
      },
      (error) => (this.errorMessage = <any>error)
    );
  }
}
