import {
  Component,
  OnInit,
  AfterViewInit,
  ViewEncapsulation,
} from '@angular/core';
import { AuthService } from 'src/app/services/authService';
declare var $: any;
declare var Morris: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  totalUsers ;
  tutor = 0 ;
  client = 0;
  constructor(private authService : AuthService)
  {
    this.GetUsersNumber();

   }

  GetUsersNumber()
  {
    this.authService.selectUsers().subscribe((data: any[]) => {

     data['rows'].forEach(element =>{
       if (element.userType === 'tutor') {this.tutor ++ ; }
       if (element.userType === 'client') {this .client ++;}
     });
     this.totalUsers = this.tutor + this.client ;
    });
      return 0 ;
  }

  ngOnInit(): void {


  }

}
