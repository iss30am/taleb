import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { user } from '../models/user';
import { professorProfiles } from '../models/professorProfiles';
import { AppComponent } from '../app.component';
import { AnySrvRecord } from 'dns';
import { userInfo } from 'os';

@Injectable({ providedIn: 'root' })
export class AuthService {
  [x: string]: any;
  private currentUserSubject: BehaviorSubject<user>
  public currentUser: Observable<user>;

  token: string;
  //  url = 'http://localhost:5001/api/auth/authenticate';
  url = 'http://talebbg-dev.ap-southeast-1.elasticbeanstalk.com/';

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<user>(JSON.parse(JSON.stringify(localStorage.getItem('currentUser'))));
    this.currentUser = this.currentUserSubject.asObservable();
  }
  public get currentUserValue(): user {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string) {

    return this.http.post<any>(this.url + "api/auth", { email, password })
      .pipe(map(user => {
        localStorage.setItem('currentUser', user);
        localStorage.setItem('token', user.token);
        console.log('auth', user.token);
        localStorage.setItem('userId', user.id);

        this.currentUserSubject.next(user);
        AppComponent.auth = true;
        AppComponent.Uname = user.name;
        return user;
      }));

  }

  public register(user: user | number): Observable<user> {
    return this.http.post<user>(this.url + "api/authnew", user);
  }
  public clientregister(user: user | number): Observable<user> {
    return this.http.post<user>(this.url + "api/authnewclient", user);
  }


  checkToken(id: number, token) {

    return this.http.post<any>(this.url + "api/tokenverif", { id, token })
      .pipe(map(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        AppComponent.auth = true;
        AppComponent.Uname = user.name;
        return user;
      }));

  }

otp(country, PhoneNumber) {

  return this.http.post<any>(this.url + "api/otp", { country, PhoneNumber })
    .pipe(map(user => {
      if (user.success) {
        AppComponent.auth = true;
        AppComponent.Uname = user.name;
        localStorage.setItem('currentUser', user);
        console.log('auth2', user);
        // localStorage.setItem('token', user.token);
        // localStorage.setItem('userId', user.id);
        this.currentUserSubject.next(user);
      }

      return user;
    }));

}
  approuved(id: number, token, isApprouved) {

    return this.http.post<any>(this.url + "api/approuved", { id, token, isApprouved })
      .pipe(map(user => {

        return user;
      }));

  }
  authotp(id, otp) {

    return this.http.post<any>(this.url + "api/authotp", { id, otp })
      .pipe(map(user => {

        return user;
      }));

  }

  logout() {
    console.log('logout auth ', 'null');
    localStorage.setItem('currentUser', null);
    this.currentUserSubject.next(null);
    this.router.navigate(['/']);
    localStorage.setItem('currentUser', null);
    console.log('auth3', null);
    localStorage.setItem('token', null);
    localStorage.setItem('userId', null);
    AppComponent.auth = true;
    AppComponent.Uname = null;
  }


  public blockUser(user): Observable<any> {
    return this.http.post<any>(this.url + "api/blockuser", user);
  }

  public getCurrency(cur): Observable<any> {
    return this.http.get<any>('https://free.currconv.com/api/v7/convert?q=USD_'+cur+'&compact=ultra&apiKey=93427c73a129ddfe04c6',{});
  }


  public selectUsers(): Observable<any> {
    return this.http.post<any>(this.url + 'api/selectUsers',{});
  }

  public selectProfessorsProfiles(): Observable<any> {
    return this.http.post<any>(this.url + 'api/selectProfessorsProfiles',{});
  }

  public selectProfessorsProfilesid(id): Observable<professorProfiles> {
    return this.http.post<professorProfiles>(this.url + 'api/selectProfessorsProfilesid', { id: id });
  }

  public selectProfessorsCertificationsprofessorsId(id): Observable<any> {
    return this.http.post<any>(this.url + 'api/selectProfessorsCertificationsprofessorsId', { professorsId: id });
  }

  public changeProfessorsProfiles(professorProfiles): Observable<professorProfiles> {
    return this.http.post<professorProfiles>(this.url + "api/changeProfessorsProfiles", professorProfiles);
  }
  public addProfessorsCertifications(professorProfiles): Observable<any> {
    return this.http.post<any>(this.url + "api/addProfessorsCertifications", professorProfiles);
  }



  uploadfile(token, file, type) {


    let credentials = {
      token, file, type

    };
    console.log(credentials)
    return this.http.post<any>(this.url + "api/s3push", credentials);
  }


   public selectbooksclientid(clientId): Observable<any> {
    return this.http.post<any>(this.url + 'api/selectbooksclientid', { clientId });
  }
  public selectbooks(): Observable<any> {
    return this.http.post<any>(this.url + 'api/selectbooks',{});
  }

  public convertCurrency(convertedCurrency : string):Observable<any>{
    const params = new HttpParams().set('username', 'prodev6241003').set('password', '32jdojh1hjt4vodop4dnqm2lhf');
    return this.http.get<any>('https://xecdapi.xe.com/v1/convert_to.json/?to=USD&from="+convertedCurrency+"&amount=1',{params});
  }



  public changeUsersProfiles(user): Observable<any> {
    return this.http.post<any>(this.url + "api/changeUsers", user);
  }

  public selectClientsProfilesClientId(): Observable<AnySrvRecord> {
    return this.http.post<any>(this.url + "api/selectClientsProfilesClientId",{} );
  }

  public selectUsersid(id): Observable<any> {
    return this.http.post<any>(this.url + "api/selectUsersid",{id} );
  }


  public addFavorites(clientId,professorsId,token): Observable<any> {
    return this.http.post<any>(this.url + "api/addFavorites", {clientId,professorsId,token});
  }
  public selectFavoritesclientId(clientId): Observable<any> {
    return this.http.post<any>(this.url + "api/selectFavoritesclientId",{clientId} );
  }

  public selectFavorites(): Observable<any> {
    return this.http.post<any>(this.url + "api/selectFavorites",{} );
  }

  public deleteFavorites(id,token): Observable<any> {
    return this.http.post<any>(this.url + "api/deleteFavorites", {id,token});
  }
}
