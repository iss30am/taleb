import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { level } from '../models/level';
import { professorLevels } from '../models/professorLevels';
import { professorListModel } from '../models/professorListModel';
import { matiere } from '../models/matiere';
import { category } from '../models/category';
import { book } from '../models/book';
@Injectable({ providedIn: 'root' })
export class EducationService {

  constructor(private http: HttpClient) { }
  public url = 'http://talebbg-dev.ap-southeast-1.elasticbeanstalk.com/';



  public addMatiere(matiere: matiere): Observable<matiere> {
    return this.http.post<matiere>(this.url + "api/addMatiers", matiere);
  }



  public addTarifs(tarif): Observable<any> {
    return this.http.post<matiere>(this.url + "api/addTarifs", tarif);
  }
  public getMatiers(): Observable<matiere[]> {
    return this.http.post<matiere[]>(this.url + 'api/selectMatiers', '');
  }

  public changeMatiers(matiere: matiere): Observable<matiere> {
    return this.http.post<matiere>(this.url + "api/changeMatiers", matiere);
  }


  public deleteMatier(matiere: matiere): Observable<matiere> {
    return this.http.post<matiere>(this.url + "api/deleteMatiers", matiere);
  }


  public selectProfessorsProfileslevelId(levelId: any): Observable<any> {
    return this.http.post<any>(this.url + "api/selectProfessorsProfileslevelId", { levelId: levelId });
  }
  public selectbooksDateDebOfBook(DateDebOfBookMin, DateDebOfBookMax): Observable<any> {
    return this.http.post<any>(this.url + "api/selectbooksDateDebOfBook", { DateDebOfBookMin, DateDebOfBookMax });
  }
  public selectbooksprofessorsid(professorsId): Observable<any> {
    return this.http.post<any>(this.url + "api/selectbooksprofessorsid", { professorsId });
  }
  public selectTarifsprofessorsId(id): Observable<any> {
    return this.http.post<any>(this.url + "api/selectTarifsprofessorsId", { id });
  }
  public selectTarifs(): Observable<any> {
    return this.http.post<any>(this.url + "api/selectTarifs", {  });
  }
  public deleteTarifs(id): Observable<any> {
    return this.http.post<any>(this.url + "api/deleteTarifs", id);
  }



  public getprofessorLevels(): Observable<professorLevels[]> {
    return this.http.get<professorLevels[]>(this.url + "/api/professorlevel/professorslevels");
  }


  public getprofessorsList(): Observable<professorListModel[]> {
    return this.http.get<professorListModel[]>(this.url + "/api/professorlevel/professorsList");
  }



  // LEVEL METHODES

  public addLevels(level: level): Observable<level> {
    return this.http.post<level>(this.url + "api/addLevels", level);
  }
  public changeLevels(level: level): Observable<level> {
    return this.http.post<level>(this.url + "api/changeLevels", level);
  }
  public deleteLevels(level: level): Observable<level> {
    return this.http.post<level>(this.url + "api/deleteLevels", level);
  }
  public getLevels(): Observable<level[]> {
    return this.http.post<level[]>(this.url + 'api/selectLevels', '');
  }



  // CATEGORY METHODES
  
  public fulltarif(id: any): Observable<any> {
    const body = { professorsId: id };
    return this.http.post<category>(this.url + "api/fulltarif", body);
  }
  public selectProfessorsProfilesid(id: any): Observable<any> {
    const body = { id: id };
    return this.http.post<category>(this.url + "api/selectProfessorsProfilesid", body);
  }
  public addCategory(category: category): Observable<category> {
    return this.http.post<category>(this.url + "api/addCategory", category);
  }
  public changeCategory(category: category): Observable<category> {
    return this.http.post<category>(this.url + "api/addCategory", category);
  }
  public deleteCategory(category: category): Observable<category> {
    return this.http.post<category>(this.url + "api/deleteCategory", category);
  }
  public getCategory(): Observable<category[]> {
    return this.http.post<category[]>(this.url + 'api/selectCategory', '');
  }
  public selectbooksclientid(clientId): Observable<any> {
    const body = { clientId: clientId };
    return this.http.post<any>(this.url + "api/selectbooksclientid",body);
  }

  public changebooks(book : book): Observable<book> {
    return this.http.post<book>(this.url + "api/changebooks", book);
  }


}
