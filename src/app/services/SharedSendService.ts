import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';
@Injectable({ providedIn: 'root' })
export class SharedSendService {
  private subject = new Subject<string>();
  constructor() { }

  sendClickEvent(chaine: any) {
    this.subject.next(chaine);
  }
  getEvent(): Observable<any> {
    return this.subject.asObservable();
  }
}
