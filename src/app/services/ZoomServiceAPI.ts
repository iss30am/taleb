import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { meeting } from '../models/meeting';

import { matiere } from '../models/matiere';
@Injectable({ providedIn: 'root' })
export class ZoomServiceAPI {
  constructor(private http: HttpClient) { }
  public url = 'https://api.zoom.us/v2/';


  public  createMeeting  (meeting: meeting): Observable <meeting>  {
    return this.http.post<meeting>("http://localhost:3000/api/zoom", meeting);
}

  public  addMatiere  (matiere: matiere): Observable <matiere>  {
    return this.http.post<matiere>(this.url+"api/addMatiers", matiere);
}

}


