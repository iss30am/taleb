
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';


import { SharedSendService } from 'src/app/services/SharedSendService';
import { Subscription } from 'rxjs';



import { Router, ActivatedRoute, ParamMap, Params } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormControl } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker' ;
import {MatNativeDateModule} from '@angular/material/core' ;

import { Options } from '@angular-slider/ngx-slider';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AuthService } from 'src/app/services/authService';
import { EducationService } from 'src/app/services/EducationService';

@Component({
  selector: 'app-mytutors',
  templateUrl: './mytutors.component.html',
  styleUrls: ['./mytutors.component.css']
})
export class MytutorsComponent implements OnInit {
  public mentors : any = [];
  myBooks: any;
  public selected: boolean;
  selectedChange: any;
  clickEventSubscription: Subscription;
  constructor(private authService : AuthService,private educationService : EducationService,public sharedSendService: SharedSendService,public translate : TranslateService)
   {
    this.getMentors();
 //this.mentors =[{"id":1,"professorsId":3,"general_ar":"استاذ لغة عربية للمراحل الجامعية والثانوية والابتدائية النحو والقواعد الاعراب الادب","details_ar":"استاذ لغة عربية للمراحل الجامعية والثانوية والابتدائية النحو والقواعد الاعراب الادب\r\nاستاذ لغة عربية للمراحل الجامعية والثانوية والابتدائية النحو والقواعد الاعراب الادب\r\n\r\n","urlCoverPhoto":"https://i.pinimg.com/originals/92/1a/cf/921acfda5b23c0ed5c07453ccbf947d7.jpg","urlMandatoryPhoto":"https://clueylearning.com.au/wp-content/uploads/2020/01/tutors-hero@2x.png","urlvideo":"https://www.youtube.com/watch?v=4lb-Kzb3hx0&ab_channel=Lisa%27sStudyGuides","rating_n":10,"rating":44,"details_en":"Math Tutors assist students individually or in small groups and help them improve their math skills. These instructors perform activities like developing tutoring resources, monitoring student progress, identifying areas needing improvement, helping with homework","general_en":"Math Tutors assist students individually or in small groups and help them improve their math skills. These instructors perform activities ","d7_h23":1,"d7_h22":0,"d7_h21":0,"d7_h20":0,"d7_h19":1,"d7_h18":1,"d7_h17":1,"d7_h16":1,"d7_h15":1,"d7_h14":1,"d7_h13":0,"d7_h12":0,"d7_h11":0,"d7_h10":0,"d7_h9":0,"d7_h8":1,"d7_h7":1,"d7_h6":0,"d7_h5":0,"d7_h4":0,"d7_h3":0,"d7_h2":0,"d7_h1":0,"d7_h0":0,"d6_h23":1,"d6_h22":1,"d6_h21":1,"d6_h20":0,"d6_h19":0,"d6_h18":0,"d6_h17":1,"d6_h16":1,"d6_h15":1,"d6_h14":1,"d6_h13":1,"d6_h12":1,"d6_h11":1,"d6_h10":0,"d6_h9":1,"d6_h8":1,"d6_h7":1,"d6_h6":1,"d6_h5":1,"d6_h4":0,"d6_h3":0,"d6_h2":0,"d6_h1":0,"d6_h0":0,"d5_h23":0,"d5_h22":1,"d5_h21":1,"d5_h20":1,"d5_h19":1,"d5_h18":1,"d5_h17":1,"d5_h16":1,"d5_h15":0,"d5_h14":0,"d5_h13":1,"d5_h12":1,"d5_h11":1,"d5_h10":1,"d5_h9":0,"d5_h8":0,"d5_h7":0,"d5_h6":0,"d5_h5":0,"d5_h4":0,"d5_h3":1,"d5_h2":1,"d5_h1":0,"d5_h0":0,"d4_h23":0,"d4_h22":0,"d4_h21":0,"d4_h20":0,"d4_h19":0,"d4_h18":0,"d4_h17":0,"d4_h16":0,"d4_h15":1,"d4_h14":1,"d4_h13":1,"d4_h12":1,"d4_h11":1,"d4_h10":1,"d4_h9":1,"d4_h8":0,"d4_h7":0,"d4_h6":0,"d4_h5":0,"d4_h4":0,"d4_h3":0,"d4_h2":1,"d4_h1":1,"d4_h0":1,"d3_h23":1,"d3_h22":1,"d3_h21":0,"d3_h20":0,"d3_h19":0,"d3_h18":0,"d3_h17":0,"d3_h16":0,"d3_h15":0,"d3_h14":0,"d3_h13":1,"d3_h12":1,"d3_h11":1,"d3_h10":0,"d3_h9":0,"d3_h8":0,"d3_h7":0,"d3_h6":0,"d3_h5":0,"d3_h4":0,"d3_h3":0,"d3_h2":0,"d3_h1":1,"d3_h0":1,"d2_h23":1,"d2_h22":0,"d2_h21":0,"d2_h20":1,"d2_h19":1,"d2_h18":1,"d2_h17":0,"d2_h16":1,"d2_h15":1,"d2_h14":1,"d2_h13":1,"d2_h12":0,"d2_h11":0,"d2_h10":0,"d2_h9":0,"d2_h8":0,"d2_h7":0,"d2_h6":1,"d2_h5":1,"d2_h4":1,"d2_h3":1,"d2_h2":1,"d2_h1":1,"d2_h0":1,"d1_h23":0,"d1_h22":0,"d1_h21":0,"d1_h20":0,"d1_h19":0,"d1_h18":0,"d1_h17":1,"d1_h16":1,"d1_h15":1,"d1_h14":1,"d1_h13":1,"d1_h12":1,"d1_h11":1,"d1_h10":1,"d1_h9":1,"d1_h8":1,"d1_h7":1,"d1_h6":1,"d1_h5":1,"d1_h4":1,"d1_h3":0,"d1_h2":0,"d1_h1":0,"d1_h0":0,"passport":"https://www.capacitymedia.com/Image/ServeImage?id=49603&w=780&h=442&cr=true","detailBank":"detailBank test","nameBank":"test nameBank","accountNumberBank":"000000","swiftBank":"111111","ibanBank":"22222","countryBank":"Qatar","arabic":1,"english":1,"published":1,"arabic_name":"ربية للمراحل ","english_name":"Example name","sexe":0,"Chinese":0,"Hindi":0,"Spanish":0,"Bengali":0,"Russian":0,"Portuguese":0,"Indonesian":0,"French":0,"timezone":""}];

 translate.use(localStorage.getItem("currentLanguage"));

 this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {
  console.log("translate actvayed");
  if (value === 'ar') {  this.translate.use('ar'); }
  if (value === 'en') {   this.translate.use('en'); }
  if (value === 'fr') {   this.translate.use('fr'); }
});


}

  ngOnInit(): void {

  }

  getMentors() {
    this.authService
      .selectFavoritesclientId(localStorage.getItem('userId'))
      .subscribe((data) => {
        data['rows'].forEach((element) => {
          this.authService
            .selectProfessorsProfilesid(element['professorsId'])
            .subscribe((res) => {
              res['rows'][0]['selected'] = true ;
                this.mentors.push (res['rows'][0]);
                console.log(res['rows'][0]) ;
            });
        });
      });
  }

  public FavoriteAction(mentor :any ) {

    if (mentor["selected"] == false){
        mentor["selected"] = true

       // console.log(mentor["professorsId"] +" is "+mentor["selected"]);
        this.authService.addFavorites(localStorage.getItem("userId"),mentor["professorsId"],localStorage.getItem("token")).subscribe(data=>{
       //  console.log("data -->" + data["rows"]);
        });
    }
    else {
     mentor["selected"] = false
     console.log(mentor["professorsId"] +" ID "+mentor["id"]);

     this.authService.selectFavoritesclientId(localStorage.getItem("userId")).subscribe(res=>{
       res["rows"].forEach(element => {
              console.log("ID to deleted " + element["id"]);
              if(element["professorsId"] == mentor["professorsId"] ){
               this.authService.deleteFavorites(element["id"],localStorage.getItem("token")).subscribe(data=>{
                 console.log("data -->" + data["rows"]);
               });
              }
       });
     });

    }



     this.selected = !this.selected;
     this.selectedChange.emit(this.selected);


   }

}
