import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { mytutorsRoutingModule } from './mytutors-routing.module';
import { MytutorsComponent } from './mytutors.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Daterangepicker } from 'ng2-daterangepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { createTranslateLoader } from 'src/app/app.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [MytutorsComponent],
  imports: [
    CommonModule,
    mytutorsRoutingModule,
    NgbModule,
    Daterangepicker,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
  ]
})
export class mytutorsModule { }
