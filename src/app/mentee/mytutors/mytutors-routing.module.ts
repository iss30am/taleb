import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MytutorsComponent } from './mytutors.component';

const routes: Routes = [
	{
		path: '',
		component: MytutorsComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class mytutorsRoutingModule { }
