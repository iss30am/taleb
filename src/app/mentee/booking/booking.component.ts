import { Component, OnInit, ɵɵpipeBind1 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/authService';
import { EducationService } from 'src/app/services/EducationService';
import * as $ from 'jquery';
import 'datatables.net';
import { Subject } from 'rxjs';
import { book } from 'src/app/models/book';
import { TranslateService } from '@ngx-translate/core';
import { SharedSendService } from 'src/app/services/SharedSendService';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],
})
export class BookingComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  bookings :any [];
 public  picture: string;
  clickEventSubscription: any;
  constructor(private authService : AuthService,private educationService : EducationService,public translate : TranslateService,public sharedSendService: SharedSendService)
   {
    this.getBooks();
    translate.use(localStorage.getItem("currentLanguage"));
    this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {
      if (value === 'ar') {  this.translate.use('ar'); }
      if (value === 'en') {   this.translate.use('en'); }
      if (value === 'fr') {   this.translate.use('fr'); }
    });
  }
  ngOnInit(): void {

  }
getBooks(){
  this.dtOptions = {
    pagingType: 'full_numbers',
    pageLength: 10
  };
  this.authService.selectbooksclientid(26).subscribe(data=>{
    this.bookings = data["rows"];
    this.bookings.forEach(element => {
        element["Price"] = (Number (element["Price"]) *  Number (localStorage.getItem("valueofUSD"))).toFixed(2) + " "+localStorage.getItem("userCurrency");

        //Get hours of Bookings
        var d = new Date(Number (element.DateDebOfBook));
        var d2 = new Date(Number (element.DateDebOfBook));
         d2.setHours(d2.getHours() + Number(element["numberOfHours"]) );
      if (d.getHours().toString().length == 1){
       element["hourDeb"]="0"+d.getHours();
      }
      else {
        element["hourDeb"]=d.getHours();
      }
      if(d.getMinutes().toString().length ==1)
      {
        element["hourDeb"]=element["hourDeb"]+":"+"0"+d.getMinutes();
      }
      else{
        element["hourDeb"]=element["hourDeb"]+":"+d.getMinutes();
      }

      if (d2.getHours().toString().length == 1){
        element["hourFin"]="0"+d2.getHours();
       }
       else {
         element["hourFin"]=d2.getHours();
       }
       if(d2.getMinutes().toString().length ==1)
       {
         element["hourFin"]=element["hourFin"]+":"+"0"+d2.getMinutes();
       }
       else{
         element["hourFin"]=element["hourFin"]+":"+d2.getMinutes();
       }
           //Status of book
           if (element["statusOfBook"] == "cancelled" ){element["cancelled"] = true}


           // Get languages of ever Tutor
        this.authService.selectProfessorsProfilesid(element["professorsId"]).subscribe(res=>{
          if(res["rows"][0]["arabic"] != 0){element["arabicFlag"] = true}
          if(res["rows"][0]["english"] != 0){element["englishFlag"] = true}
          if(res["rows"][0]["bengali"] != 0){element["bengaliFlag"] = true}
          if(res["rows"][0]["chinese"] != 0){element["chineseFlag"] = true}
          if(res["rows"][0]["frensh"] != 0){element["frenshFlag"] = true}
          if(res["rows"][0]["hindi"] != 0){element["hindiFlag"] = true}
          if(res["rows"][0]["indonesian"] != 0){element["indonesianFlag"] = true}
          if(res["rows"][0]["portugal"] != 0){element["portugalFlag"] = true}
          if(res["rows"][0]["russian"] != 0){element["russianFlag"] = true}
          if(res["rows"][0]["spanish"] != 0){element["spanishFlag"] = true}
        });

         //Get general Tutor Informations
        this.authService.selectUsersid(element["professorsId"]).subscribe(res=>{
          element["photo"] = res["rows"][0]["photo"];
          element["tutorName"] = res["rows"][0]["firstName"]+" "+res["rows"][0]["lastName"];
        });

        // Get informations about  & Level
        this.educationService.getLevels().subscribe(res=>{
          var level =  res["rows"].find(item=>item["id"] == element["levelId"]);
         // console.log("Matiere Id -->" + level["matiereId"]);
            this.educationService.getMatiers().subscribe(res1=>{
              var matiere = res1["rows"].find(item=>item["id"] == level["matiereId"]);
             // console.log("Matiere" + JSON.stringify(matiere));
              element["subject"] = matiere["englishName"]+" : "+level["LevelNameEnglish"];
            })
        })


   });
   this.dtTrigger.next();
  });
}

cancelBook(b :any){
b["statusOfBook"]="cancelled";
b["DateDebOfBook"]="";
const b1 = new book(b["id"],b["professorsId"],b["refBook"],b["clientId"],b["DateDebOfBook"],b["numberOfHours"],b["levelId"],b["statusOfBook"],b["Transaction"],b["videoCall"],b["paymentStatus"],b["paymentWallet"],b["Price"],b["paymentOnline"],b["dh1"],b["dh2"],b["CreationDate"],b["_Delete"],localStorage.getItem('token'));
this.educationService.changebooks(b1).subscribe(data=>{
  console.log(JSON.stringify(data));
});

}


}
