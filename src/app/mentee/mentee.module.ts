import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenteeRoutingModule } from './mentee-routing.module';
import { MenteeComponent } from './mentee.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { createTranslateLoader } from '../app.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';





@NgModule({
  declarations: [MenteeComponent, SidemenuComponent,],
  imports: [CommonModule,
     MenteeRoutingModule,
      NgbModule ,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: createTranslateLoader,
          deps: [HttpClient]
        }
      }),
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class MenteeModule { }
