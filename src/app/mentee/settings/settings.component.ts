import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/authService';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { user } from 'src/app/models/user';
import { ToastrService } from 'ngx-toastr';
import { TranslateService,TranslatePipe } from '@ngx-translate/core';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {

  currentUser : user ;
  firstName: string;
  email: string;
  lastName: string;
  PhoneNumber: string;
  birthDate: Date;
  country: string;
  isApprouved: boolean;
  active: string;
  countrybirth: string;
  countryliving: string;
  sexe: string;
  timezone: string;
  error: any;
  photo: any;
  phoneNumberConfirmed: any;
  emailConfirmed: any;

  constructor(private authService : AuthService,
    private toastr: ToastrService,
    public translate : TranslateService) {
      const browserLang: string = translate.getBrowserLang();
      translate.use(browserLang.match(/en|fr|ar/) ? browserLang : "en");
       this.initInformations();
      }

  ngOnInit(): void
  {
    this.initInformations();

  }

  save() {
    this.currentUser = new user(parseInt (localStorage.getItem('userId')), this.firstName,this.email,this.firstName,this.lastName,this.PhoneNumber,true,true,localStorage.getItem('userType'),this.birthDate,this.country,this.isApprouved,this.active,localStorage.getItem('token'),this.countrybirth,this.countryliving,this.sexe,this.timezone);
   console.log(this.currentUser);
    this.currentUser.photo = this.photo;
    this.authService.changeUsersProfiles(this.currentUser).subscribe(data=>{
    console.log(data);


      if (data.success)
      {

        this.toastr.success('', 'Update success!');
        this.initInformations();
      }
      else
      {
        this.toastr.error(data.message, 'Login failed!');
      }
      error => {

        this.toastr.error(error, 'Login failed!');
        this.error = error;
        console.log(error);
      }
    })


    window.scroll(0, 0);
  }
  upload(event, data, row) {
    this.changeListener(event, data, row)
  }

  async changeListener(event, data, row) {
    var inputValue = event.target

    var file: File = inputValue.files[0];

    let myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {

      this.authService.uploadfile(localStorage.getItem('token'), myReader.result, file.name.substr(file.name.lastIndexOf('.') + 1))
        .subscribe((res) => {

          if (row) {
            this.photo = res.Location
            console.log(JSON.stringify(row, res.Location))
          } else {
            return res.Location
          }

        })
      console.log(JSON.stringify(data))
    }
    myReader.readAsDataURL(file);
  }
public initInformations()
{
    this.authService.selectUsersid(localStorage.getItem('userId')).subscribe(data =>{
      var element = data.rows[0];

            console.log("*******>"+JSON.stringify(element)  );

            this.firstName = element.firstName ;
            this.lastName = element.lastName ;
            this.birthDate = element.birthDate;
            this.email = element.email;
            this.PhoneNumber = element.PhoneNumber;
            this.photo = element.photo;
            this.country = element.country ;
            this.isApprouved = element.isApprouved;
            this.phoneNumberConfirmed = element.phoneNumberConfirmed;
            this.emailConfirmed = element.emailConfirmed;
            this.active = element.active;
            this.countrybirth = element.countrybirth;
            this.countryliving = element.countryliving;
            this.sexe = element.sexe ;
            this.timezone = element.timezone;


    })
}

}
