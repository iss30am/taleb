import { Component, OnInit } from '@angular/core';
import { Event, NavigationStart, Router } from '@angular/router';
import { TranslateDefaultParser, TranslateService} from '@ngx-translate/core';
import { AppComponent } from 'src/app/app.component';
import { SharedSendService } from 'src/app/services/SharedSendService';


@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css'],
})
export class SidemenuComponent implements OnInit {
  name;
  base;
  page;
  splitVal;
  clickEventSubscription: any;
  constructor(
    private router: Router,public sharedSendService: SharedSendService,public translate : TranslateService)
    {

      translate.use(localStorage.getItem("currentLanguage"));
      this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {
        if (value === 'ar') {  this.translate.use('ar'); }
        if (value === 'en') {   this.translate.use('en'); }
        if (value === 'fr') {   this.translate.use('fr'); }
      });
    }
  ngOnInit(): void {
    this.splitVal = this.router.url.split('/');
    this.base = this.splitVal[1];
    this.page = this.splitVal[2];
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.splitVal = event.url.split('/');
        this.base = this.splitVal[1];
        this.page = this.splitVal[2];
      }
    });
  }

  logout() {
    console.log('logout side bar', 'null');
    localStorage.setItem('currentUser', null);

    this.router.navigate(['/']);
    localStorage.setItem('currentUser', null);
    localStorage.setItem('token', null);
    localStorage.setItem('userId', null);
    AppComponent.auth = false;
    AppComponent.Uname = null;
  }


}
