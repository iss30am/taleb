import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { CalendarModule } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { CalendarRoutingModule } from '../calendar/calendar-routing.module';


@NgModule({
  declarations: [DashboardComponent],
  imports: [CommonModule, DashboardRoutingModule, NgxDropzoneModule, ReactiveFormsModule, FormsModule, CalendarModule.forRoot({
    provide: DateAdapter,
    useFactory: adapterFactory
  }),],
})
export class DashboardModule { }
