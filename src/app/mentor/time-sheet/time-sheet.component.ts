import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/authService';

@Component({
  selector: 'app-time-sheet',
  templateUrl: './time-sheet.component.html',
  styleUrls: ['./time-sheet.component.css']
})
export class TimeSheetComponent {
  id: string;
  currentProfessorProfile: any;

  constructor(
    private authService: AuthService,
    private toastr: ToastrService,
  ) {
    this.id = localStorage.getItem('userId');
    this.getProfessorsProfiles(this.id);
  }


  public getProfessorsProfiles(id) {
    this.authService.selectProfessorsProfilesid(id).subscribe((data: any) => {
      this.currentProfessorProfile = data["rows"][0];
      this.currentProfessorProfile.token = localStorage.getItem('token');
      console.log("currentProfessorProfile  " + JSON.stringify(this.currentProfessorProfile));
    }), error => {
      console.log("ERROR:" + error);
    };
  }
  save() {


    var tab = ['d7_h23', 'd7_h22', 'd7_h21', 'd7_h20', 'd7_h19', 'd7_h18', 'd7_h17', 'd7_h16', 'd7_h15', 'd7_h14', 'd7_h13', 'd7_h12', 'd7_h11', 'd7_h10', 'd7_h9', 'd7_h8', 'd7_h7', 'd7_h6', 'd7_h5', 'd7_h4', 'd7_h3', 'd7_h2', 'd7_h1', 'd7_h0', 'd6_h23', 'd6_h22', 'd6_h21', 'd6_h20', 'd6_h19', 'd6_h18', 'd6_h17', 'd6_h16', 'd6_h15', 'd6_h14', 'd6_h13', 'd6_h12', 'd6_h11', 'd6_h10', 'd6_h9', 'd6_h8', 'd6_h7', 'd6_h6', 'd6_h5', 'd6_h4', 'd6_h3', 'd6_h2', 'd6_h1', 'd6_h0', 'd5_h23', 'd5_h22', 'd5_h21', 'd5_h20', 'd5_h19', 'd5_h18', 'd5_h17', 'd5_h16', 'd5_h15', 'd5_h14', 'd5_h13', 'd5_h12', 'd5_h11', 'd5_h10', 'd5_h9', 'd5_h8', 'd5_h7', 'd5_h6', 'd5_h5', 'd5_h4', 'd5_h3', 'd5_h2', 'd5_h1', 'd5_h0', 'd4_h23', 'd4_h22', 'd4_h21', 'd4_h20', 'd4_h19', 'd4_h18', 'd4_h17', 'd4_h16', 'd4_h15', 'd4_h14', 'd4_h13', 'd4_h12', 'd4_h11', 'd4_h10', 'd4_h9', 'd4_h8', 'd4_h7', 'd4_h6', 'd4_h5', 'd4_h4', 'd4_h3', 'd4_h2', 'd4_h1', 'd4_h0', 'd3_h23', 'd3_h22', 'd3_h21', 'd3_h20', 'd3_h19', 'd3_h18', 'd3_h17', 'd3_h16', 'd3_h15', 'd3_h14', 'd3_h13', 'd3_h12', 'd3_h11', 'd3_h10', 'd3_h9', 'd3_h8', 'd3_h7', 'd3_h6', 'd3_h5', 'd3_h4', 'd3_h3', 'd3_h2', 'd3_h1', 'd3_h0', 'd2_h23', 'd2_h22', 'd2_h21', 'd2_h20', 'd2_h19', 'd2_h18', 'd2_h17', 'd2_h16', 'd2_h15', 'd2_h14', 'd2_h13', 'd2_h12', 'd2_h11', 'd2_h10', 'd2_h9', 'd2_h8', 'd2_h7', 'd2_h6', 'd2_h5', 'd2_h4', 'd2_h3', 'd2_h2', 'd2_h1', 'd2_h0', 'd1_h23', 'd1_h22', 'd1_h21', 'd1_h20', 'd1_h19', 'd1_h18', 'd1_h17', 'd1_h16', 'd1_h15', 'd1_h14', 'd1_h13', 'd1_h12', 'd1_h11', 'd1_h10', 'd1_h9', 'd1_h8', 'd1_h7', 'd1_h6', 'd1_h5', 'd1_h4', 'd1_h3', 'd1_h2', 'd1_h1', 'd1_h0'];
    tab.forEach(element => {

      this.currentProfessorProfile[element] = this.checker(this.currentProfessorProfile[element])
      console.log(this.currentProfessorProfile[element])
    });

    this.authService.changeProfessorsProfiles(this.currentProfessorProfile).subscribe(data => {
      this.getProfessorsProfiles(this.id);
      if (data['success']) {
        this.toastr.success('', 'profile changed');
      }
      else {
        this.toastr.error(data['message'], 'Changed failed!');
      }




    })

  }
  checker(d) {
    d ? d = 1 : d = 0
    return d
  }
  consolelog(x) {
    console.log(x)
  }
}
