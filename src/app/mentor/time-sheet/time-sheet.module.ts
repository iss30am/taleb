import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimeSheetRoutingModule } from './time-sheet-routing.mudule';

import { NgxDropzoneModule } from 'ngx-dropzone';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TimeSheetComponent } from './time-sheet.component';
@NgModule({
  declarations: [TimeSheetComponent],
  imports: [CommonModule, TimeSheetRoutingModule, NgxDropzoneModule, ReactiveFormsModule, FormsModule],
})
export class TimeSheetModule {
}
