import { Component, OnInit } from '@angular/core';
import {
  Event,
  NavigationStart,
  Router,
  ActivatedRoute,
} from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { AuthService } from 'src/app/services/authService';

import { CommonServiceService } from './../../common-service.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css'],
})
export class SidemenuComponent implements OnInit {
  public currentProfessorProfile: any;
  name;
  splitVal;
  base;
  page;
  id: string;
  constructor(
    private router: Router,
    public commonService: CommonServiceService,
    private authService: AuthService,
  ) {
    this.id = localStorage.getItem('userId');
    this.getProfessorsProfiles(this.id);


  }

  ngOnInit(): void {
    this.splitVal = this.router.url.split('/');
    this.base = this.splitVal[1];
    this.page = this.splitVal[2];
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.splitVal = event.url.split('/');
        this.base = this.splitVal[1];
        this.page = this.splitVal[2];
      }
    });
  }

  logout() {
    console.log('logout auth ', 'null');
    localStorage.setItem('currentUser', null);
    this.router.navigate(['/']);
    localStorage.setItem('currentUser', null);
    localStorage.setItem('token', null);
    localStorage.setItem('userId', null);
    AppComponent.auth = false;
    AppComponent.Uname = null;
  }

  navigate(name) {
    this.name = name;
    this.commonService.nextmessage(name);
  }



  public getProfessorsProfiles(id) {
    this.authService.selectProfessorsProfilesid(id).subscribe((data: any) => {
      this.currentProfessorProfile = data["rows"][0];
      this.currentProfessorProfile.token = localStorage.getItem('token');
      console.log("currentProfessorProfile " + this.currentProfessorProfile);
    }), error => {
      console.log("ERROR:" + error);
    };
  }
}
