import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { level } from 'src/app/models/level';
import { EducationService } from 'src/app/services/EducationService';

@Component({
  selector: 'app-scheduletiming',
  templateUrl: './scheduletiming.component.html',
  styleUrls: ['./scheduletiming.component.css']
})
export class ScheduletimingComponent implements OnInit {
  id: string;
  token: string;
  ngOnInit(): void {
  }
  constructor(
    private educationService: EducationService,
    private toastr: ToastrService,
    public router: Router,) {

    this.id = localStorage.getItem('userId');
    this.token = localStorage.getItem('token');

    this.listMatiers()
  }

  showAdd() {
    this.show = true;

    this.matiereId = 0

  }
  show = false;
  hideAdd() {
    this.show = false;

  }

 
  categoryId
  categorys;
  matiereId;
  matiers;
  LevelId;
  Levels;
  price
  listgCategory() {
    this.educationService.getCategory().subscribe((data) => {
      this.categorys = data["rows"];

      console.log('categorys', this.categorys)
   
      this.selectTarifsprofessorsId(this.id)
    });
  }
  listMatiers() {
    this.educationService.getMatiers().subscribe((data: any) => {
      this.matiers = data["rows"];
      console.log('matiers', this.matiers)
      this.listgCategory()
      // this.matiers.forEach(element => {

      //  });
    });
  }
  selectTarifsprofessorsId(id) {
    this.educationService.selectTarifsprofessorsId(id).subscribe((data: any) => {
      this.Tarifs = data["rows"];
      console.log(this.Tarifs)

      this.Tarifs.forEach(element => {
        element.name = this.getLevel(element.levelId)
        console.log()
      });
      console.log('Tarifs',this.Tarifs)
    });
  }
  Tarifs = []


  listlevels() {
    this.educationService.getLevels().subscribe((data: any) => {
      this.Levels = data["rows"];
      console.log('Levels', this.Levels)

      this.Levels.forEach(element => {
        element['isEdit'] = false;
        element.matier = this.getNameOfMatierId(element.matiereId)

        element.category = this.getNameOfcategoryId(element.categoryId)



      })

    

      // this.matiers.forEach(element => {

      //  });
    });
  }

 
  getLevel(id) {
    var rslt = null;
    this.categorys.forEach(element => {
      if (element.id == id) {
        rslt = element.CategoryNameEnglish + '  | ' + element.CategoryNameArabic
        this.matiers.forEach(matier => {
          if (element.matiereId == matier.id) {
            // rslt = rslt + ' - ' + matier.englishName + '  | ' + matier.arabicName

            rslt = matier.englishName + ' - ' + element.CategoryNameEnglish + '  | ' + matier.arabicName + ' - ' + element.CategoryNameArabic

          }
        })
        return rslt
      }

    });

    return rslt
  }
  getNameOfMatierId(id) {
    var rslt = null;
    this.matiers.forEach(element => {
      if (element.id == id) {
        rslt = element
        return element
      }

    });

    return rslt
  }
  getNameOfcategoryId(id) {
    var rslt = null;
    this.categorys.forEach(element => {
      if (Number(element.id) == Number(id)) {
        rslt = element
        return element
      }

    });

    return rslt
  }


  getCategories() {
    this.educationService.getCategory().subscribe((data) => {
      this.categorys = data["rows"];
      // this.categorys.forEach(element => {

      // });
     this.selectTarifsprofessorsId(this.id)
    });
  }
  Add() {
    console.log({ professorsId: this.id, levelId: this.LevelId, price: this.price, endOfValidity: 0 })
    this.educationService.addTarifs({ professorsId: this.id, levelId: this.categoryId, price: this.price, endOfValidity: 0, token: this.token }).subscribe((data) => {
      this.id = localStorage.getItem('userId');
      this.selectTarifsprofessorsId(this.id)
      this.hideAdd()
    })

  }

  delete(id) {
    this.educationService.deleteTarifs({ id: id, token: this.token }).subscribe((data) => {
      this.id = localStorage.getItem('userId');
      this.token = localStorage.getItem('token');
      this.selectTarifsprofessorsId(this.id)
      this.hideAdd()
    })

  }





}
