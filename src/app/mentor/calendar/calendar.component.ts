import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CalendarEvent, CalendarEventAction } from 'angular-calendar';
import { Subscription } from 'rxjs';
import { CommonServiceService } from 'src/app/common-service.service';
import { AuthService } from 'src/app/services/authService';
import { EducationService } from 'src/app/services/EducationService';
import { SharedSendService } from 'src/app/services/SharedSendService';
import { startOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addWeeks, subWeeks, addMonths, subMonths, addMinutes } from 'date-fns';


const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent {
  id: string;
  token: string;
  Levels: any;
  categorys: any;
  dtTrigger: any;
  matiers: any;

  constructor(public authService: AuthService, public educationService: EducationService, public translate: TranslateService) {
    this.id = localStorage.getItem('userId');
    this.token = localStorage.getItem('token');
    this.listMatiers()



  }
  listgCategory() {
    this.educationService.getCategory().subscribe((data) => {
      this.categorys = data["rows"];

      console.log('categorys', this.categorys)
      this.listlevels();
      this.dtTrigger.next();
    });
  }
  listMatiers() {
    this.educationService.getMatiers().subscribe((data: any) => {
      this.matiers = data["rows"];
      console.log('matiers', this.matiers)
      this.listgCategory()
      // this.matiers.forEach(element => {

      //  });
    });
  }
  listlevels() {
    this.educationService.getLevels().subscribe((data: any) => {
      this.Levels = data["rows"];
      console.log('Levels', this.Levels)

      this.Levels.forEach(element => {
        element.LevelNameEnglish = element.LevelNameEnglish
      });
      this.getbooks()
    });
  }
  getNameOfLevelId(id) {
    var rslt = null;
    this.Levels.forEach(element => {
      if (element.id == id) {
        rslt = this.getNameOfMatierId(element.matiereId) + ' ,  ' + this.getNameOfcategoryId(element.categoryId) + ' ,  ' + element.LevelNameEnglish
      }

    });

    return rslt
  }
  getNameOfMatierId(id) {
    var rslt = null;
    this.matiers.forEach(element => {
      if (element.id == id) {
        rslt = element.englishName
      }

    });

    return rslt
  }
  getNameOfcategoryId(id) {
    var rslt = null;
    this.categorys.forEach(element => {
      if (element.id == id) {
        rslt = element.CategoryNameEnglish
      }

    });

    return rslt
  }
  async getbooks() {
    this.viewDate = new Date();
    this.educationService.selectbooksprofessorsid(this.id).subscribe(async (res) => {

      var books = res.rows;
      console.log(books)
      this.events = []

      for (let index = 0; index < books.length; index++) {
        try {


          let book = books[index];
          let start = (new Date())
          let end = ((new Date()))
          start.setTime(Number(book.DateDebOfBook))
          end.setTime(Number(book.DateDebOfBook))
          end = addMinutes(end, 50)
          let title = start.getHours() + ' ' + this.getNameOfLevelId(book.levelId)
          var color = colors.red;
          if (this.viewDate < start) {
            color = colors.yellow;
          } else if (this.viewDate > end) {
            color = colors.blue;
          }

          this.events.push(
            {
              start: start,
              end: end,
              title: title,
              color: color,
              actions: this.actions,
              meta: book
            }
          )

        } catch (error) {
          console.error(error)
        }
      };




    })
    console.log(this.events)
  }

  view: string = 'month';

  viewDate: Date = new Date();

  actions: CalendarEventAction[] = [{
    label: '<i class="fa fa-fw fa-pencil"></i>',
    onClick: ({ event }: { event: CalendarEvent }): void => {
      console.log('Edit event', event);
    }
  }, {
    label: '<i class="fa fa-fw fa-times"></i>',
    onClick: ({ event }: { event: CalendarEvent }): void => {
      this.events = this.events.filter(iEvent => iEvent !== event);
    }
  }];

  events: CalendarEvent[] = []
  h = [

    {
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'A 3 day event',
      color: colors.red,
      actions: this.actions
    }, {
      start: startOfDay(new Date()),
      title: 'An event with no end date',
      color: colors.yellow,
      actions: this.actions
    }, {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'A long event that spans 2 months',
      color: colors.blue
    }];

  activeDayIsOpen: boolean = true;

  increment(): void {

    const addFn: any = {
      day: addDays,
      week: addWeeks,
      month: addMonths
    }[this.view];

    this.viewDate = addFn(this.viewDate, 1);

  }

  decrement(): void {

    const subFn: any = {
      day: subDays,
      week: subWeeks,
      month: subMonths
    }[this.view];

    this.viewDate = subFn(this.viewDate, 1);

  }

  today(): void {
    this.viewDate = new Date();
  }

  dayClicked({ date, events }: { date: Date, events: CalendarEvent[] }): void {

    if (isSameMonth(date, this.viewDate)) {
      if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }
}
