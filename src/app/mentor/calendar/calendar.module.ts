import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarComponent } from './calendar.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
@NgModule({
  declarations: [CalendarComponent],
  imports: [CommonModule, CalendarRoutingModule, NgxDropzoneModule, ReactiveFormsModule, FormsModule, CalendarModule.forRoot({
    provide: DateAdapter,
    useFactory: adapterFactory
  }),],
})
export class CalendaryModule {

}

