


import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';
import { Observable } from 'rxjs';
import { professorProfiles } from 'src/app/models/professorProfiles';
import { AuthService } from 'src/app/services/authService';
import { FormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var $: any;

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent {

  public professorSettings: any;
  public currentProfessorProfile: any;
  arabic_name: any;
  dataRegister: any = {};
  english_name: any;
  details_en: any;
  details_ar: any;
  general_ar: any;
  general_en: any;
  nameBank: any;
  accountNumberBank: any;
  swiftBank: any;
  ibanBank: any;
  countryBank: any;
  error: any;
  id: any;
  token: string;
  Certifications: any;
  CertificationsAdd: any;
  constructor(
    private authService: AuthService,
    private toastr: ToastrService,

  ) {
    this.id = localStorage.getItem('userId');
    this.token = localStorage.getItem('token');
    this.getProfessorsProfiles(this.id);
    this.selectProfessorsCertificationsprofessorsId(this.id);

  }


  public getProfessorsProfiles(id) {
    this.authService.selectProfessorsProfilesid(id).subscribe((data: any) => {
      this.currentProfessorProfile = data["rows"][0];
      this.currentProfessorProfile.token = localStorage.getItem('token');
      console.log("currentProfessorProfile " + this.currentProfessorProfile);
    }), error => {
      console.log("ERROR:" + error);

    };
  }
  public selectProfessorsCertificationsprofessorsId(id) {
    console
    this.authService.selectProfessorsCertificationsprofessorsId(id).subscribe((data: any) => {
      this.Certifications = data["rows"];
      console.log('Certifications', id, this.Certifications)

    }), error => {
      console.log("ERROR:" + error);
    };
  }

  save() {

    this.authService.changeProfessorsProfiles(this.currentProfessorProfile).subscribe(
      data => {
        console.log("data " + data);
        this.getProfessorsProfiles(this.currentProfessorProfile.professorsId)
        this.dataRegister = data;
        if (this.dataRegister.success) {
          this.toastr.success('', 'profile changed');
        }
        else {
          this.toastr.error(this.dataRegister.message, 'Changed failed!');
        }
      },
      error => {

        this.toastr.error(error, 'Changed failed!');
        this.error = error;
        // console.log(error);
      }
    );

  }

  nameOfCertification
  endDateValidity
  urlOfCertification
  addProfessorsCertifications() {
    // this.currentProfessorProfile.nameOfCertification = this.nameOfCertification
    // this.currentProfessorProfile.endDateValidity = this.endDateValidity
    // this.currentProfessorProfile.urlOfCertification = this.CertificationsAdd
    // console.log(this.currentProfessorProfile)

    console.log({ professorsId: this.currentProfessorProfile.professorsId, nameOfCertification: this.nameOfCertification, endDateValidity: this.endDateValidity, urlOfCertification: this.CertificationsAdd, token: this.token })
    this.authService.addProfessorsCertifications({ professorsId: this.currentProfessorProfile.professorsId, nameOfCertification: this.nameOfCertification, endDateValidity: this.endDateValidity, urlOfCertification: this.CertificationsAdd, token: this.token }).subscribe(
      data => {
        console.log("data " + data);
        this.selectProfessorsCertificationsprofessorsId(this.currentProfessorProfile.professorsId)
        this.dataRegister = data;
        if (this.dataRegister.success) {
          this.CertificationsAdd = false
          this.nameOfCertification = ''
          this.endDateValidity = ''
          this.urlOfCertification = ''
          this.toastr.success('', 'Certifications changed');
        }
        else {
          this.toastr.error(this.dataRegister.message, ' failed!');
        }
      },
      error => {

        this.toastr.error(error, 'Changed failed!');
        this.error = error;
        // console.log(error);
      }
    );

  }


  upload(event, data, row) {
    this.changeListener(event, data, row)
  }

  async changeListener(event, data, row) {
    var inputValue = event.target

    var file: File = inputValue.files[0];

    let myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {

      this.authService.uploadfile(this.token, myReader.result, file.name.substr(file.name.lastIndexOf('.') + 1))
        .subscribe((res) => {

          if (row) {
            this.CertificationsAdd = res.Location

          } else {
            return res.Location
          }

        })
      console.log(JSON.stringify(data))
    }
    myReader.readAsDataURL(file);
  }
  submit(stat, text) {
    if (confirm(text)) {
      this.currentProfessorProfile.published = stat
      this.save();

    }
  }
}

