import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZoomRoutingModule } from './zoom-routing.module';
import { ZoomComponent } from './zoom.component';
import { TooltipModule } from 'ng2-tooltip-directive';

@NgModule({
  declarations: [ZoomComponent],
  imports: [CommonModule, ZoomRoutingModule, TooltipModule],
})
export class ZoomModule {}
