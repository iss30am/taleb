import { Component, OnInit } from '@angular/core';
import { ZoomModule } from './zoom.module';

import { ZoomServiceAPI } from '../services/zoomServiceAPI';
import { HttpClient } from '@angular/common/http';
import { meeting } from '../models/meeting';
import { meetingSettings } from '../models/meetingSettings';



@Component({
  selector: 'app-zoom',
  templateUrl: './zoom.component.html',
  styles: [
  ]
})
export class ZoomComponent implements OnInit {

public  accessToken   = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6InZXOGJreUtwU1l5M0ZpLVpiQXBmUHciLCJleHAiOjE2MTUyMTQyMzEsImlhdCI6MTYxNTIwOTAyM30.n5Og-qZFuvpFxjNtPkY-MFb9dQuSyfqgvejnCBTnDpA';
public meetConfig : any ;
public CreateMeetConfig : any ;
public signature : any ;
public ele : any ;
container :any = false ;

  constructor(private http: HttpClient ,private zoomService : ZoomServiceAPI)
  {
 // id of Zoom :    -HBj8bcnTuacnQ5KqRcE1g
  }

  joinMeeting(MeetingNumber,MeetingPassword)
  {
    window.location.href = "http://localhost:4201/zoom?id="+MeetingNumber+"&pwd="+MeetingPassword;
  }

 LeaveMeeting()
 {
   this.container = true ;
  //ZoomMtg.leaveMeeting({});
 }



  ngOnInit(): void {

  }
  SetConfigCreateMeeting(){
    this.CreateMeetConfig = {
      apiKey : 'vW8bkyKpSYy3Fi-ZbApfPw',
      apiSecret : 'MydkAmmdu1wlQ3ULQaeDYxZOmOYpAJsc7gX6',
    }
  }

 CreateMeeting(){

 const ms = new  meetingSettings(
   true,true,false,false,true,false,true,true,1,'both','local',false,'none','none',["Qatar"],true
 );
 const m = new meeting(
   "Reunion de test ","2","2020-03-10T15:40:00Z","60","sahnouni.bayrem@gmail.com","","","Taleb Meeting",ms
 )



 var request : any ;
 this.zoomService.createMeeting(m).subscribe((data) => {
  //console.log(data) ;


  console.log("-->" + data["meetings"][0].id);
  console.log("-->" + (data["meetings"][0].join_url).substring(42,(data["meetings"][0].join_url).length));
 }, (err) => {
   console.error(err);
 });
 }
}
