import { settings } from "cluster";
import { meetingSettings } from "./meetingSettings";

export class meeting {
  [x: string]: any;
  constructor(
      public topic : string ,
      public type : string,
      public start_time : string,
      public duration : string,
      public schedule_for : string,
      public  timezone : string,
      public  password : string,
      public  agenda : string,
      public  settings : meetingSettings

  ){
  }
}
