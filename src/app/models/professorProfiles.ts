export class professorProfiles {
  public id: number;
  public arabic_name: number;
  public english_name: string;
  public professorsId: string;
  public general_ar: string;
  public urlCoverPhoto: string;
  public urlMandatoryPhoto: string;
  public urlvideo: boolean;
  public rating_n: boolean;
  public rating: string;
  public details_en: string;
  public details_ar: string;
  public general_en: string;
  public passport: boolean;
  public nameBank: string;
  public accountNumberBank: string;
  public swiftBank: string;
  public ibanBank: string;
  public countryBank: string;
  public token: string
  constructor(

  ) {
  }
}
