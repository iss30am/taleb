export class meetingSettings {
  [x: string]: any;
  constructor(

      public host_video : boolean,
      public participant_video : boolean,
      public cn_meeting : boolean,
      public in_meeting : boolean,
      public join_before_host : boolean,
      public mute_upon_entry : boolean,
      public watermark : boolean,
      public use_pmi : boolean,
      public approval_type : number,

      public audio : string,
      public auto_recording : string,
      public enforce_login : boolean,
      public enforce_login_domains : string,
      public alternative_hosts : string,
      public global_dial_in_countries : string[],
      public registrants_email_notification : boolean

  ){
  }
}
