export class matiere {
    [x: string]: any;
    constructor(
        public Id: Number,
        public englishName: string,
        public arabicName: string,
        public englishDescription: string,
        public arabicDescription: string,

        public token: string

    ) {
    }
}
