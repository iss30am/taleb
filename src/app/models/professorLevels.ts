export class professorLevels {
    constructor(
        public Id: Number,
        public professorId: Number,
        public levelId: Number,
        public price: Number
    ) {
    }
}