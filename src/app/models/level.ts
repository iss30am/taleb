export class level {
    constructor(
        public id: Number,
        public categoryId: Number,
        public matiereId: Number,
        public LevelNameEnglish: string,
        public LevelNameArabic: string,
        public LevelDescriptionEnglish: string,
        public LevelDescriptionArabic: string,
        public token: string

    ) {
    }
}