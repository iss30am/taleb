export class professorListModel {
    constructor(
        public Id: Number,
        public professorId: Number,
        public levelId: Number,
        public price: Number,
        public rating: number,
        public numberPersonRating: number,
        public general: string,
        public details: string,
        public firstName: string,
        public lastName: string,
        public urlMandatoryPhoto: string
    ) {
    }
}