export class book {
  constructor(
      public id: Number,
      public professorsId: Number,
      public refBook: string,
      public clientId: Number,
      public DateDebOfBook: Number,
      public numberOfHours: Number,
      public levelId: Number,
      public statusOfBook: string,
      public Transaction: string,
      public videoCall: string,
      public paymentStatus: string,
      public paymentWallet: string,
      public Price: string,
      public paymentOnline: string,
      public dh1: Number,
      public dh2: Number,
      public CreationDate: Number,
      public _Delete: string,
      public token : string

  ) {
  }
}
