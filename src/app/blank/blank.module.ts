import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlankComponent } from './blank.component';
import { BlankRoutingModule } from './blank-routing.module';
import { FormsModule } from '@angular/forms';
import { NgOtpInputModule } from 'ng-otp-input';

@NgModule({
  declarations: [BlankComponent],
  imports: [NgOtpInputModule,FormsModule,CommonModule, BlankRoutingModule],
})
export class BlankModule { }
