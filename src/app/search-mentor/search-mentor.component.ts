import { category } from './../models/category';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

import { CommonServiceService } from '../common-service.service';
import { SharedSendService } from 'src/app/services/SharedSendService';
import { Subscription } from 'rxjs';

import { EducationService } from '../services/EducationService';
import { professorListModel } from '../models/professorListModel';
import { Router, ActivatedRoute, ParamMap, Params } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormControl } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker' ;
import {MatNativeDateModule} from '@angular/material/core' ;

import { Options } from '@angular-slider/ngx-slider';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AuthService } from '../services/authService';

@Component({
  selector: 'app-search-mentor',
  templateUrl: './search-mentor.component.html',
  styleUrls: ['./search-mentor.component.css'],
})
export class SearchMentorComponent implements OnInit, OnDestroy {
  isFavorite : boolean ;
minDate :   Date;
maxDate  :  Date;

dateMin = new Date() ;
dateMax = new Date(new Date().setMonth(new Date().getMonth() + 1))

  valueofUSD : number  = parseFloat(localStorage.getItem("valueofUSD"))  ;
  userCurrency : string;
  @Input() selected: boolean;
  @Output() selectedChange = new EventEmitter<boolean>();
  specialityList: any = [];
  type;
  specialist = '';
  speciality;
  selDate;
  clickEventSubscription: Subscription;
  id: number;
  public mentors = [];
  public Arr = Array;
  level: any;
  myParam: any;
  Tarifs: any;
  _Matiers: any;
  max: any;
  min: any;
  rateControl = new FormControl();
  rangeValues: any[];
  from: any;
  to: any;
  minValue: number = 50;
  maxValue: number = 200;
  options: Options = {
    floor: 0,
    ceil: 250
  };
  searchedProfessorsList: any;
  public _selectdLanguage: any;
  selected_date: Date;
  selected_date2: Date;

  constructor(
    public commonService: CommonServiceService,
    public router: Router,
    public translate: TranslateService,
    public sharedSendService: SharedSendService,
    public route: ActivatedRoute,
    private educationService: EducationService,
    private authService : AuthService,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) {
    translate.use(localStorage.getItem("currentLanguage"));

    this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {
    translate.use(value);
     this.ngOnInit();
    });



    this.activatedRoute.queryParams.subscribe(params => {
      this._Matiers = this.route.snapshot.paramMap.get("level")
       // Print the parameter to the console.
      console.log('activatedRoute',this._Matiers)
    });
    const currentYear = new Date().getFullYear();



    this.firstday = (new Date()).getDay()
    for (let index = this.firstday; index <= 6; index++) {
      this.week.push({ index: index, day: this.days[index], day_a: this.days_a[index] })
    }

    for (let index = 0; index < this.firstday; index++) {
      this.week.push({ index: index, day: this.days[index], day_a: this.days_a[index] })
    }
    console.log(this.week)
    var date = new Date();
    var date2 = new Date();
    var date3 = new Date();
    var date4 = new Date();
    var date5 = new Date();
    date.setHours(0)
    date.setMinutes(1)
    date2.setHours(0);
    date2.setMinutes(1);
    date2.setDate(date2.getDate() + 7);
    date3.setHours(0);
    date3.setMinutes(1);
    date3.setDate(date3.getDate() + 14);
    date4.setHours(0);
    date4.setMinutes(1);
    date4.setDate(date4.getDate() + 21);
    date5.setHours(0);
    date5.setMinutes(1);
    date5.setDate(date5.getDate() + 28);

    this.dates = [
      { timestart: date.getTime(), timeend: date2.getTime(), day: this.days[date.getDay()], day_a: this.days_a[date.getDay()], month: this.month[date.getMonth()], month_ar: this.month_ar[date.getMonth()], date: date, datee: date2 },
      { timestart: date2.getTime(), timeend: date3.getTime(), day: this.days[date2.getDay()], day_a: this.days_a[date2.getDay()], month: this.month[date2.getMonth()], month_ar: this.month_ar[date2.getMonth()], date: date2, datee: date3 },
      { timestart: date3.getTime(), timeend: date4.getTime(), day: this.days[date3.getDay()], day_a: this.days_a[date3.getDay()], month: this.month[date3.getMonth()], month_ar: this.month_ar[date3.getMonth()], date: date3, datee: date4 },
      { timestart: date4.getTime(), timeend: date5.getTime(), day: this.days[date4.getDay()], day_a: this.days_a[date4.getDay()], month: this.month[date4.getMonth()], month_ar: this.month_ar[date4.getMonth()], date: date4, datee: date5 }

    ]

  //  this.getProfessorsList(this.Level)
    this.getMatiers()
    this.userCurrency  =localStorage.getItem("userCurrency");
  }
  images = [
    {
      path: 'assets/img/features/feature-01.jpg',
    },
    {
      path: 'assets/img/features/feature-02.jpg',
    },
    {
      path: 'assets/img/features/feature-03.jpg',
    },
    {
      path: 'assets/img/features/feature-04.jpg',
    },
  ];
  ngOnInit(): void {
    this.getCurrencyAmount();

  }

  getCurrencyAmount(){
    const headers = new HttpHeaders()
    .set("Accept","*/*")
    .set('cache-control', 'no-cache')
    .set('content-type', 'application/json')
const body = {
  account_id: 'prodev6241003',
  api_key: '32jdojh1hjt4vodop4dnqm2lhf'
}
 console.log  ( this.http.post("https://xecdapi.xe.com/v1/convert_from.json/?from=USD&to=CAD,EUR&amount=110.23",body,{headers : headers}).subscribe(res=>res) ) ;
  }

  getCategory(): void {
    this.educationService.getCategory().subscribe((data) => {

      this.Category = data["rows"];
      console.log('Category', this.Category)
     this.selectTarifs()


    });
  }
  getMatiers(): void {
    this.educationService.getMatiers().subscribe((data) => {

      this.Matiers = data["rows"];
      console.log('Matiers', this.Matiers)

      this.getCategory()
    });
  }
  getLevels(): void {
    this.educationService.getLevels().subscribe((data) => {

      this.Levels = data["rows"];
      console.log('Levels', this.Levels)

      this.getCategory()
    });
  }





clear(){
this._selectdLanguage="";
this.minValue = 50 ; this.maxValue = 200 ;
this.dateMin = new Date() ;
this.dateMax = new Date(new Date().setMonth(new Date().getMonth() + 1))

}



  search() {




     this.searchedProfessorsList = null ;


    this.mentors.forEach(mentor=>{
     if(mentor[this._selectdLanguage] == 1)
     {
        this.searchedProfessorsList.push(mentor);
     }


     }
     );



console.log("searchedProfessorsList : " + this.searchedProfessorsList);

  }


  bookAppointment(id) {
    this.router.navigateByUrl('/mentee/booking?id=' + id);
  }
  getProfessorsList(id): void {
    console.log('getProfessorsList', id)
    this.educationService.selectProfessorsProfileslevelId(id).subscribe((data) => {


      this.mentors = data.rows;

       data["rows"].forEach(element => {
        element["selected"] = false ;
        this.authService.selectFavoritesclientId(localStorage.getItem("userId")).subscribe(res=>{

          res["rows"].forEach(Favoriteelement => {
         //   element["selected"] = true ;

            if(Favoriteelement["professorsId"] == element["professorsId"]){
              element["selected"] = true ;
               console.log("selected Professor -->"+element["professorsId"])
             }

          });
        })
       });


      this.min = 99999999
      this.max = -10
      this.mentors.forEach(tutor => {
        tutor.segment = 'General'
        tutor.Tarifs = []
        tutor.TarifsID = []
        tutor.min = 99999999
        tutor.max = -10

        this.Tarifs.forEach(Tarif => {
          if (Number(Tarif.professorsId) == Number(tutor.professorsId) ) {
            console.log(Number(Tarif.professorsId) ,Number(tutor.professorsId))
            if (Tarif.price > tutor.max) {
              tutor.max = Tarif.price
             }
            if (Tarif.price < tutor.min) {
              tutor.min = Tarif.price
            }
            if (Tarif.price > this.max) {
              this.max = Tarif.price
            }
            if (Tarif.price < this.min) {
              this.min = Tarif.price
            }



            tutor.Tarifs.push(Tarif)
            tutor.TarifsID.push(Tarif.id)
           }


        });


      });
      try {
        this.rateControl = new FormControl("", [this.max, this.min])

        this.minValue = this.min;
        this.maxValue = this.max;

        this.options.floor = this.min;
        this.options.ceil = this.max;
        console.log(id, data)
      } catch (error) {

      }


      this.weekcalendar(0);
      console.log("************0000" )
    }, (err) => console.log("************"+err));
  }

  rateInput
  ngOnDestroy() {
  }

    public FavoriteAction(mentor :any ) {

   if (mentor["selected"] == false){
       mentor["selected"] = true

      // console.log(mentor["professorsId"] +" is "+mentor["selected"]);
       this.authService.addFavorites(localStorage.getItem("userId"),mentor["professorsId"],localStorage.getItem("token")).subscribe(data=>{
      //  console.log("data -->" + data["rows"]);
       });
   }
   else {
    mentor["selected"] = false
    console.log(mentor["professorsId"] +" ID "+mentor["id"]);

    this.authService.selectFavoritesclientId(localStorage.getItem("userId")).subscribe(res=>{
      res["rows"].forEach(element => {
             console.log("ID to deleted " + element["id"]);
             if(element["professorsId"] == mentor["professorsId"] ){
              this.authService.deleteFavorites(element["id"],localStorage.getItem("token")).subscribe(data=>{
                console.log("data -->" + data["rows"]);
              });
             }
      });
    });

   }



    this.selected = !this.selected;
    this.selectedChange.emit(this.selected);


  }


  Levels: any;
  Level: any;
  Matiers: any;
  Category: any;
  segment: any;
  firstday: any;
  week = [];
  days = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
  days_a = ['الأحَد', 'إثنين', 'ثلاثاء', 'أربعاء', 'خميس', 'جمعة', 'سبت'];
  month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  month_ar = ["يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
  dates: any;
  selecteddate = 0
  selectedtext: any;




  isEnglish() {
    return this.translate.currentLang != 'ar';
  }



  getLevel(id) {
    var rslt = null;
    this.Category.forEach(element => {
      if (element.id == id) {

          element.name = element.CategoryNameEnglish + '  | ' + element.CategoryNameArabic
        return element
      }

    });

    return rslt
  }
  selectTarifs() {
    this.educationService.selectTarifs().subscribe((data: any) => {
      this.Tarifs = data["rows"];
     // console.log(this.Tarifs)

      this.Tarifs.forEach(element => {
        element.Category = this.getLevel(element.levelId)
        console.log()
      });
      this.getProfessorsList(this._Matiers);
      console.log(this.Tarifs)
    });
  }

  emptyday = { h0: 0, h1: 0, h2: 0, h3: 0, h4: 0, h5: 0, h6: 0, h7: 0, h8: 0, h9: 0, h10: 0, h11: 0, h12: 0, h13: 0, h14: 0, h15: 0, h16: 0, h17: 0, h18: 0, h19: 0, h20: 0, h21: 0, h22: 0, h23: 0 }
  weekcalendar(n) {
    var D = new Date(this.dates[n].date)
    var E = new Date(this.dates[n].datee)
    this.selectedtext = { ar: D.getDate() + '' + this.month_ar[D.getMonth()] + ' - ' + E.getDate() + '' + this.month_ar[E.getMonth()], en: D.getDate() + '' + this.month[D.getMonth()] + ' - ' + E.getDate() + '' + this.month[E.getMonth()] }
    this.selected_date = D;
    this.selected_date2 = E;



    this.educationService.selectbooksDateDebOfBook(D, E).subscribe(async (res) => {
      var books = res.rows;
     books.forEach(book => {
       var DateDebOfBook = new Date(book.DateDebOfBook )
       book.dh2 = 'd' + Number(DateDebOfBook.getDay() + 1) + '_h' + Number(DateDebOfBook.getHours())
       console.log(book.dh1)

      })
      this.mentors.forEach(tutor => {
        tutor.week = [];

        var week = JSON.parse(JSON.stringify(this.week));

        week.forEach(day => {

          var emptyday = JSON.parse(JSON.stringify(this.emptyday));

          if (day.index == 6) {

            emptyday.h0 = this.checkBook(tutor, books, tutor.d7_h0, 'd7_h0')
            emptyday.h1 = this.checkBook(tutor, books, tutor.d7_h1, 'd7_h1')
            emptyday.h2 = this.checkBook(tutor, books, tutor.d7_h2, 'd7_h2')
            emptyday.h3 = this.checkBook(tutor, books, tutor.d7_h3, 'd7_h3')
            emptyday.h4 = this.checkBook(tutor, books, tutor.d7_h4, 'd7_h4')
            emptyday.h5 = this.checkBook(tutor, books, tutor.d7_h5, 'd7_h5')
            emptyday.h6 = this.checkBook(tutor, books, tutor.d7_h6, 'd7_h6')
            emptyday.h7 = this.checkBook(tutor, books, tutor.d7_h7, 'd7_h7')
            emptyday.h8 = this.checkBook(tutor, books, tutor.d7_h8, 'd7_h8')
            emptyday.h9 = this.checkBook(tutor, books, tutor.d7_h9, 'd7_h9')

            emptyday.h10 = this.checkBook(tutor, books, tutor.d7_h10, 'd7_h10')
            emptyday.h11 = this.checkBook(tutor, books, tutor.d7_h11, 'd7_h11')
            emptyday.h12 = this.checkBook(tutor, books, tutor.d7_h12, 'd7_h12')
            emptyday.h13 = this.checkBook(tutor, books, tutor.d7_h13, 'd7_h13')
            emptyday.h14 = this.checkBook(tutor, books, tutor.d7_h14, 'd7_h14')
            emptyday.h15 = this.checkBook(tutor, books, tutor.d7_h15, 'd7_h15')
            emptyday.h16 = this.checkBook(tutor, books, tutor.d7_h16, 'd7_h16')
            emptyday.h17 = this.checkBook(tutor, books, tutor.d7_h17, 'd7_h17')
            emptyday.h18 = this.checkBook(tutor, books, tutor.d7_h18, 'd7_h18')
            emptyday.h19 = this.checkBook(tutor, books, tutor.d7_h19, 'd7_h19')

            emptyday.h20 = this.checkBook(tutor, books, tutor.d7_h20, 'd7_h20')
            emptyday.h21 = this.checkBook(tutor, books, tutor.d7_h21, 'd7_h21')
            emptyday.h22 = this.checkBook(tutor, books, tutor.d7_h22, 'd7_h22')
            emptyday.h23 = this.checkBook(tutor, books, tutor.d7_h23, 'd7_h23')





          }

          if (day.index == 0) {
            emptyday.h0 = this.checkBook(tutor, books, tutor.d1_h0, 'd1_h0')
            emptyday.h1 = this.checkBook(tutor, books, tutor.d1_h1, 'd1_h1')
            emptyday.h2 = this.checkBook(tutor, books, tutor.d1_h2, 'd1_h2')
            emptyday.h3 = this.checkBook(tutor, books, tutor.d1_h3, 'd1_h3')
            emptyday.h4 = this.checkBook(tutor, books, tutor.d1_h4, 'd1_h4')
            emptyday.h5 = this.checkBook(tutor, books, tutor.d1_h5, 'd1_h5')
            emptyday.h6 = this.checkBook(tutor, books, tutor.d1_h6, 'd1_h6')
            emptyday.h7 = this.checkBook(tutor, books, tutor.d1_h7, 'd1_h7')
            emptyday.h8 = this.checkBook(tutor, books, tutor.d1_h8, 'd1_h8')
            emptyday.h9 = this.checkBook(tutor, books, tutor.d1_h9, 'd1_h9')

            emptyday.h10 = this.checkBook(tutor, books, tutor.d1_h10, 'd1_h10')
            emptyday.h11 = this.checkBook(tutor, books, tutor.d1_h11, 'd1_h11')
            emptyday.h12 = this.checkBook(tutor, books, tutor.d1_h12, 'd1_h12')
            emptyday.h13 = this.checkBook(tutor, books, tutor.d1_h13, 'd1_h13')
            emptyday.h14 = this.checkBook(tutor, books, tutor.d1_h14, 'd1_h14')
            emptyday.h15 = this.checkBook(tutor, books, tutor.d1_h15, 'd1_h15')
            emptyday.h16 = this.checkBook(tutor, books, tutor.d1_h16, 'd1_h16')
            emptyday.h17 = this.checkBook(tutor, books, tutor.d1_h17, 'd1_h17')
            emptyday.h18 = this.checkBook(tutor, books, tutor.d1_h18, 'd1_h18')
            emptyday.h19 = this.checkBook(tutor, books, tutor.d1_h19, 'd1_h19')

            emptyday.h20 = this.checkBook(tutor, books, tutor.d1_h20, 'd1_h20')
            emptyday.h21 = this.checkBook(tutor, books, tutor.d1_h21, 'd1_h21')
            emptyday.h22 = this.checkBook(tutor, books, tutor.d1_h22, 'd1_h22')
            emptyday.h23 = this.checkBook(tutor, books, tutor.d1_h23, 'd1_h23')
          }


          if (day.index == 1) {
            emptyday.h0 = this.checkBook(tutor, books, tutor.d2_h0, 'd2_h0')
            emptyday.h1 = this.checkBook(tutor, books, tutor.d2_h1, 'd2_h1')
            emptyday.h2 = this.checkBook(tutor, books, tutor.d2_h2, 'd2_h2')
            emptyday.h3 = this.checkBook(tutor, books, tutor.d2_h3, 'd2_h3')
            emptyday.h4 = this.checkBook(tutor, books, tutor.d2_h4, 'd2_h4')
            emptyday.h5 = this.checkBook(tutor, books, tutor.d2_h5, 'd2_h5')
            emptyday.h6 = this.checkBook(tutor, books, tutor.d2_h6, 'd2_h6')
            emptyday.h7 = this.checkBook(tutor, books, tutor.d2_h7, 'd2_h7')
            emptyday.h8 = this.checkBook(tutor, books, tutor.d2_h8, 'd2_h8')
            emptyday.h9 = this.checkBook(tutor, books, tutor.d2_h9, 'd2_h9')

            emptyday.h10 = this.checkBook(tutor, books, tutor.d2_h10, 'd2_h10')
            emptyday.h11 = this.checkBook(tutor, books, tutor.d2_h11, 'd2_h11')
            emptyday.h12 = this.checkBook(tutor, books, tutor.d2_h12, 'd2_h12')
            emptyday.h13 = this.checkBook(tutor, books, tutor.d2_h13, 'd2_h13')
            emptyday.h14 = this.checkBook(tutor, books, tutor.d2_h14, 'd2_h14')
            emptyday.h15 = this.checkBook(tutor, books, tutor.d2_h15, 'd2_h15')
            emptyday.h16 = this.checkBook(tutor, books, tutor.d2_h16, 'd2_h16')
            emptyday.h17 = this.checkBook(tutor, books, tutor.d2_h17, 'd2_h17')
            emptyday.h18 = this.checkBook(tutor, books, tutor.d2_h18, 'd2_h18')
            emptyday.h19 = this.checkBook(tutor, books, tutor.d2_h19, 'd2_h19')

            emptyday.h20 = this.checkBook(tutor, books, tutor.d2_h20, 'd2_h20')
            emptyday.h21 = this.checkBook(tutor, books, tutor.d2_h21, 'd2_h21')
            emptyday.h22 = this.checkBook(tutor, books, tutor.d2_h22, 'd2_h22')
            emptyday.h23 = this.checkBook(tutor, books, tutor.d2_h23, 'd2_h23')
          }


          if (day.index == 2) {
            emptyday.h0 = this.checkBook(tutor, books, tutor.d3_h0, 'd3_h0')
            emptyday.h1 = this.checkBook(tutor, books, tutor.d3_h1, 'd3_h1')
            emptyday.h2 = this.checkBook(tutor, books, tutor.d3_h2, 'd3_h2')
            emptyday.h3 = this.checkBook(tutor, books, tutor.d3_h3, 'd3_h3')
            emptyday.h4 = this.checkBook(tutor, books, tutor.d3_h4, 'd3_h4')
            emptyday.h5 = this.checkBook(tutor, books, tutor.d3_h5, 'd3_h5')
            emptyday.h6 = this.checkBook(tutor, books, tutor.d3_h6, 'd3_h6')
            emptyday.h7 = this.checkBook(tutor, books, tutor.d3_h7, 'd3_h7')
            emptyday.h8 = this.checkBook(tutor, books, tutor.d3_h8, 'd3_h8')
            emptyday.h9 = this.checkBook(tutor, books, tutor.d3_h9, 'd3_h9')

            emptyday.h10 = this.checkBook(tutor, books, tutor.d3_h10, 'd3_h10')
            emptyday.h11 = this.checkBook(tutor, books, tutor.d3_h11, 'd3_h11')
            emptyday.h12 = this.checkBook(tutor, books, tutor.d3_h12, 'd3_h12')
            emptyday.h13 = this.checkBook(tutor, books, tutor.d3_h13, 'd3_h13')
            emptyday.h14 = this.checkBook(tutor, books, tutor.d3_h14, 'd3_h14')
            emptyday.h15 = this.checkBook(tutor, books, tutor.d3_h15, 'd3_h15')
            emptyday.h16 = this.checkBook(tutor, books, tutor.d3_h16, 'd3_h16')
            emptyday.h17 = this.checkBook(tutor, books, tutor.d3_h17, 'd3_h17')
            emptyday.h18 = this.checkBook(tutor, books, tutor.d3_h18, 'd3_h18')
            emptyday.h19 = this.checkBook(tutor, books, tutor.d3_h19, 'd3_h19')

            emptyday.h20 = this.checkBook(tutor, books, tutor.d3_h20, 'd3_h20')
            emptyday.h21 = this.checkBook(tutor, books, tutor.d3_h21, 'd3_h21')
            emptyday.h22 = this.checkBook(tutor, books, tutor.d3_h22, 'd3_h22')
            emptyday.h23 = this.checkBook(tutor, books, tutor.d3_h23, 'd3_h23')
          }


          if (day.index == 3) {
            emptyday.h0 = this.checkBook(tutor, books, tutor.d4_h0, 'd4_h0')
            emptyday.h1 = this.checkBook(tutor, books, tutor.d4_h1, 'd4_h1')
            emptyday.h2 = this.checkBook(tutor, books, tutor.d4_h2, 'd4_h2')
            emptyday.h3 = this.checkBook(tutor, books, tutor.d4_h3, 'd4_h3')
            emptyday.h4 = this.checkBook(tutor, books, tutor.d4_h4, 'd4_h4')
            emptyday.h5 = this.checkBook(tutor, books, tutor.d4_h5, 'd4_h5')
            emptyday.h6 = this.checkBook(tutor, books, tutor.d4_h6, 'd4_h6')
            emptyday.h7 = this.checkBook(tutor, books, tutor.d4_h7, 'd4_h7')
            emptyday.h8 = this.checkBook(tutor, books, tutor.d4_h8, 'd4_h8')
            emptyday.h9 = this.checkBook(tutor, books, tutor.d4_h9, 'd4_h9')

            emptyday.h10 = this.checkBook(tutor, books, tutor.d4_h10, 'd4_h10')
            emptyday.h11 = this.checkBook(tutor, books, tutor.d4_h11, 'd4_h11')
            emptyday.h12 = this.checkBook(tutor, books, tutor.d4_h12, 'd4_h12')
            emptyday.h13 = this.checkBook(tutor, books, tutor.d4_h13, 'd4_h13')
            emptyday.h14 = this.checkBook(tutor, books, tutor.d4_h14, 'd4_h14')
            emptyday.h15 = this.checkBook(tutor, books, tutor.d4_h15, 'd4_h15')
            emptyday.h16 = this.checkBook(tutor, books, tutor.d4_h16, 'd4_h16')
            emptyday.h17 = this.checkBook(tutor, books, tutor.d4_h17, 'd4_h17')
            emptyday.h18 = this.checkBook(tutor, books, tutor.d4_h18, 'd4_h18')
            emptyday.h19 = this.checkBook(tutor, books, tutor.d4_h19, 'd4_h19')

            emptyday.h20 = this.checkBook(tutor, books, tutor.d4_h20, 'd4_h20')
            emptyday.h21 = this.checkBook(tutor, books, tutor.d4_h21, 'd4_h21')
            emptyday.h22 = this.checkBook(tutor, books, tutor.d4_h22, 'd4_h22')
            emptyday.h23 = this.checkBook(tutor, books, tutor.d4_h23, 'd4_h23')

          }


          if (day.index == 4) {
            emptyday.h0 = this.checkBook(tutor, books, tutor.d5_h0, 'd5_h0')
            emptyday.h1 = this.checkBook(tutor, books, tutor.d5_h1, 'd5_h1')
            emptyday.h2 = this.checkBook(tutor, books, tutor.d5_h2, 'd5_h2')
            emptyday.h3 = this.checkBook(tutor, books, tutor.d5_h3, 'd5_h3')
            emptyday.h4 = this.checkBook(tutor, books, tutor.d5_h4, 'd5_h4')
            emptyday.h5 = this.checkBook(tutor, books, tutor.d5_h5, 'd5_h5')
            emptyday.h6 = this.checkBook(tutor, books, tutor.d5_h6, 'd5_h6')
            emptyday.h7 = this.checkBook(tutor, books, tutor.d5_h7, 'd5_h7')
            emptyday.h8 = this.checkBook(tutor, books, tutor.d5_h8, 'd5_h8')
            emptyday.h9 = this.checkBook(tutor, books, tutor.d5_h9, 'd5_h9')

            emptyday.h10 = this.checkBook(tutor, books, tutor.d5_h10, 'd5_h10')
            emptyday.h11 = this.checkBook(tutor, books, tutor.d5_h11, 'd5_h11')
            emptyday.h12 = this.checkBook(tutor, books, tutor.d5_h12, 'd5_h12')
            emptyday.h13 = this.checkBook(tutor, books, tutor.d5_h13, 'd5_h13')
            emptyday.h14 = this.checkBook(tutor, books, tutor.d5_h14, 'd5_h14')
            emptyday.h15 = this.checkBook(tutor, books, tutor.d5_h15, 'd5_h15')
            emptyday.h16 = this.checkBook(tutor, books, tutor.d5_h16, 'd5_h16')
            emptyday.h17 = this.checkBook(tutor, books, tutor.d5_h17, 'd5_h17')
            emptyday.h18 = this.checkBook(tutor, books, tutor.d5_h18, 'd5_h18')
            emptyday.h19 = this.checkBook(tutor, books, tutor.d5_h19, 'd5_h19')

            emptyday.h20 = this.checkBook(tutor, books, tutor.d5_h20, 'd5_h20')
            emptyday.h21 = this.checkBook(tutor, books, tutor.d5_h21, 'd5_h21')
            emptyday.h22 = this.checkBook(tutor, books, tutor.d5_h22, 'd5_h22')
            emptyday.h23 = this.checkBook(tutor, books, tutor.d5_h23, 'd5_h23')
          }


          if (day.index == 5) {
            emptyday.h0 = this.checkBook(tutor, books, tutor.d6_h0, 'd6_h0')
            emptyday.h1 = this.checkBook(tutor, books, tutor.d6_h1, 'd6_h1')
            emptyday.h2 = this.checkBook(tutor, books, tutor.d6_h2, 'd6_h2')
            emptyday.h3 = this.checkBook(tutor, books, tutor.d6_h3, 'd6_h3')
            emptyday.h4 = this.checkBook(tutor, books, tutor.d6_h4, 'd6_h4')
            emptyday.h5 = this.checkBook(tutor, books, tutor.d6_h5, 'd6_h5')
            emptyday.h6 = this.checkBook(tutor, books, tutor.d6_h6, 'd6_h6')
            emptyday.h7 = this.checkBook(tutor, books, tutor.d6_h7, 'd6_h7')
            emptyday.h8 = this.checkBook(tutor, books, tutor.d6_h8, 'd6_h8')
            emptyday.h9 = this.checkBook(tutor, books, tutor.d6_h9, 'd6_h9')

            emptyday.h10 = this.checkBook(tutor, books, tutor.d6_h10, 'd6_h10')
            emptyday.h11 = this.checkBook(tutor, books, tutor.d6_h11, 'd6_h11')
            emptyday.h12 = this.checkBook(tutor, books, tutor.d6_h12, 'd6_h12')
            emptyday.h13 = this.checkBook(tutor, books, tutor.d6_h13, 'd6_h13')
            emptyday.h14 = this.checkBook(tutor, books, tutor.d6_h14, 'd6_h14')
            emptyday.h15 = this.checkBook(tutor, books, tutor.d6_h15, 'd6_h15')
            emptyday.h16 = this.checkBook(tutor, books, tutor.d6_h16, 'd6_h16')
            emptyday.h17 = this.checkBook(tutor, books, tutor.d6_h17, 'd6_h17')
            emptyday.h18 = this.checkBook(tutor, books, tutor.d6_h18, 'd6_h18')
            emptyday.h19 = this.checkBook(tutor, books, tutor.d6_h19, 'd6_h19')

            emptyday.h20 = this.checkBook(tutor, books, tutor.d6_h20, 'd6_h20')
            emptyday.h21 = this.checkBook(tutor, books, tutor.d6_h21, 'd6_h21')
            emptyday.h22 = this.checkBook(tutor, books, tutor.d6_h22, 'd6_h22')
            emptyday.h23 = this.checkBook(tutor, books, tutor.d6_h23, 'd6_h23')


          }
          tutor.week.push({ today: emptyday, day: day })

        });
        console.log(tutor)


      });

    })


  }
  checkBook(tutor, books, cd, d) {
    var rslt = { o: 0, t: 'E', dh1: d, s: '' }
    if (cd) {
      rslt = { o: 1, t: 'O', dh1: d, s: '' };
      books.forEach(book => {
        if (( book.dh2 == d) && (tutor.professorsId == book.professorsId)) {
          rslt = { o: 2, t: "X", dh1: d, s: '' };
        }
      }
      )
    }
    return rslt;

  }

  nextdate() {
    if (this.selecteddate < 4) {
      this.selecteddate = this.selecteddate + 1;
      this.weekcalendar(this.selecteddate)
    }
  }
  backdate() {
    if (this.selecteddate >= 0) {
      this.selecteddate = this.selecteddate - 1;
      this.weekcalendar(this.selecteddate)
    }
  }



}
