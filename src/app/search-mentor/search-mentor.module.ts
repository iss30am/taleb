import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchMentorRoutingModule } from './search-mentor-routing.module';
import { SearchMentorComponent } from './search-mentor.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TooltipModule } from 'ng2-tooltip-directive';
import { MatDatepickerModule } from '@angular/material/datepicker' ;
import {MatNativeDateModule} from '@angular/material/core' ;
import {MatFormFieldModule} from '@angular/material/form-field';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { createTranslateLoader } from '../app.module';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [SearchMentorComponent],
  imports: [NgxSliderModule,CommonModule,
     SearchMentorRoutingModule,
      NgbModule,
      TooltipModule,
       MatButtonModule,
        MatIconModule,
         FormsModule,
         ReactiveFormsModule,
    MatDatepickerModule
    ,MatNativeDateModule
    ,MatFormFieldModule,MatInputModule ,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),



  ],
})
export class SearchMentorModule { }
