import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { clientregisterComponent } from './clientregister.component';

const routes: Routes = [
	{
		path: '',
		component: clientregisterComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class clientregisterRoutingModule { }
