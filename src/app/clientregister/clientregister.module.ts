import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { clientregisterRoutingModule } from './clientregister-routing.module';
import { clientregisterComponent } from './clientregister.component';
import { NgOtpInputModule } from 'ng-otp-input';

@NgModule({
  declarations: [clientregisterComponent],
  imports: [NgOtpInputModule, FormsModule, CommonModule, clientregisterRoutingModule],
})
export class clientregisterModule { }
