import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  AfterViewInit,
  ViewEncapsulation,
  AfterViewChecked,
  HostBinding,
  ElementRef,
  ViewChild,
} from '@angular/core';
import {
  Event,
  NavigationStart,
  Router,
  ActivatedRoute,
} from '@angular/router';
import { Location, PlatformLocation } from '@angular/common';
import { CommonServiceService } from './common-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  //changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit, AfterViewChecked {
  private isDark = false ;
  @HostBinding('class')

 /*  switchMode(isDarkMode : boolean){
 this.isDark = isDarkMode ;
  }*/


  fontSize = 14;
  @ViewChild('para', { static: true }) para: ElementRef;

  title = 'mentor';
  url;
  loadFooter = false;
  show: boolean = true;
  hideFooter: boolean = false;
  isBreadcrumb: boolean = false;
  searchBreadcrumb: boolean = true;
  splitVal;
  base = '';
  page = '';
  static auth: any;
  static Uname: void;

  constructor(
    private activeRoute: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
    public Router: Router,
    location: Location,
    public commonServic: CommonServiceService,
    platform: PlatformLocation,

  ) {
    platform.onPopState(() => {
      if (this.Router.url === '/index' || this.Router.url === '/') {
        this.show = true;
        this.hideFooter = false;
        this.isBreadcrumb = false;
        this.searchBreadcrumb = true;
      }
    });
    Router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.splitVal = event.url.split('/');
        this.base = this.splitVal[1];
        this.page = this.splitVal[2];
        if (this.base === 'admin') {
          this.isBreadcrumb = true;
          this.show = false;
          this.hideFooter = true;
          if (this.page === 'error-first' || this.page === 'error-second') {
            document.querySelector('body').classList.add('error-page');
            document.querySelector('body').classList.remove('mat-typography');
          } else {
            document.querySelector('body').classList.remove('error-page');
            document.querySelector('body').classList.add('mat-typography');
          }
        } else {
          if (
            event.url === '/register' ||
            event.url === '/login-page' ||
            event.url === '/forgot-password'
          ) {
            this.hideFooter = true;
            this.show = false;
            this.isBreadcrumb = true;
            this.searchBreadcrumb = true;
            document.querySelector('body').classList.add('account-page');
            document.querySelector('body').classList.remove('mat-typography');
          } else if (
            event.url === '/video-call' ||
            event.url === '/voice-call'
          ) {
            this.isBreadcrumb = true;
            this.searchBreadcrumb = true;
            document.querySelector('body').classList.add('call-page');
            document.querySelector('body').classList.remove('mat-typography');
          } else if (event.url === '/map-grid') {
            this.hideFooter = true;
            this.isBreadcrumb = true;
            this.searchBreadcrumb = true;
          } else if (event.url === '/message') {
            this.hideFooter = true;
            this.isBreadcrumb = true;
            this.searchBreadcrumb = true;
          } else if (event.url === '/' || event.url === '/index') {
            this.hideFooter = false;
            this.show = true;
            this.isBreadcrumb = true;
            this.searchBreadcrumb = true;
          } else if (event.url === '/message') {
            this.isBreadcrumb = true;
            this.searchBreadcrumb = true;
          } else if (event.url === '/search-mentor') {
            this.isBreadcrumb = true;
            this.searchBreadcrumb = false;
          } else {
            this.isBreadcrumb = false;
            this.searchBreadcrumb = true;
            this.hideFooter = false;
            this.show = true;
            document.querySelector('body').classList.remove('account-page');
            document.querySelector('body').classList.add('mat-typography');
            document.querySelector('body').classList.remove('call-page');
            document.querySelector('body').classList.add('mat-typography');
          }
          if (this.page !== undefined) {
            if (this.page === 'mentees') {
              this.page = 'Mentee List';
            } else if (this.page === 'booking') {
              this.page = 'My Bookings';
            } else if (this.page === 'success') {
              this.page = 'Booking Success';
            } else if (this.page === 'settings') {
              this.page = 'Profile Settings';
            } else if (this.page.includes('-') === true) {
              let x = this.page.split('-');
              if (x[1].includes('?') === true) {
                x[1] = x[1].split('?')[0];
              }
              this.page = x[0].toUpperCase() + ' ' + x[1].toUpperCase();
            }
          } else {
            if (this.base === 'blog') {
              this.page = 'Blog List';
            } else if (this.base === 'blank') {
              this.page = 'Blank Page';
            } else {
              if (this.base.includes('-') === true) {
                let x = this.base.split('-');
                if (x[1].includes('?') === true) {
                  x[1] = x[1].split('?')[0];
                }
                this.base = this.page =
                  x[0].toUpperCase() + ' ' + x[1].toUpperCase();
              } else {
                this.page = this.base;
              }
            }
          }
        }
      }
    });





  }
  ngOnInit() {
    setTimeout(() => (this.loadFooter = true), 2000);
  }
  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }
  changeFont(operator) {
    // operator === '+' ? this.fontSize++ : this.fontSize--;
     localStorage.setItem("fontsize",operator);
     this.fontSize = operator ;
     (this.para.nativeElement as HTMLParagraphElement).style.fontSize = `${this.fontSize}px`;
   }
}
