import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedSendService } from 'src/app/services/SharedSendService';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit ,OnDestroy{

  clickEventSubscription: any;
  @ViewChild('para', { static: true }) para: ElementRef;

  constructor(public translate: TranslateService,
    public sharedSendService: SharedSendService
  )
   {

    this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {
      if (value === 'Reload') { this.ngOnInit();}

    });

   }

  ngOnInit(): void {
    this.changeFont();
  }
  changeFont() {
    (this.para.nativeElement as HTMLParagraphElement).style.fontSize = `${Number (localStorage.getItem("fontsize"))}px`;
   }
   ngOnDestroy() {
    this.clickEventSubscription.unsubscribe();
  }
}
