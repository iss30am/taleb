import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterViewInit,
  Inject,
  EventEmitter,
  Output,
  ViewChild,
  TemplateRef,
  ElementRef
} from '@angular/core';
import { Event, Router, NavigationStart } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { CommonServiceService } from './../../common-service.service';
import { TranslateService } from '@ngx-translate/core';
import { SharedSendService } from 'src/app/services/SharedSendService';
import { AuthService } from 'src/app/services/authService';
import { first } from 'rxjs/operators';
import { AppComponent } from 'src/app/app.component';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';
import { interval } from 'rxjs';




import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';


import {MatSlideToggleChange, MatSlideToggleModule} from '@angular/material/slide-toggle'
import { DropDownList } from 'src/app/models/DropDownList';





@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Output()
   readonly darkModeSwitched = new EventEmitter<boolean>();

   @ViewChild('para', { static: true }) para: ElementRef;

   isAdministrator : boolean ;
   selectedValueFont:string;
   selectedLanguageNOC : string ;
   selectedLanguageCON : string ;
  defaultCurrency : DropDownList ;
  defaultCurrencyString : string = "" ;
  counterSubscription: Subscription;
  secondes: number;
  auth: boolean = false;
  isPatient: boolean = false;
  splitVal;
  base = '';
  page = '';
  name: any;
  data: any;
  urlPhotoUser : string ;
  public bellCollapsed = true;
  public userCollapsed = true;
  fontSize: any;

  constructor(

    private modalService: NgbModal,
    @Inject(DOCUMENT) private document,
    private cdr: ChangeDetectorRef,
    public router: Router,
    public commonService: CommonServiceService,
    public translate: TranslateService,
    public sharedSendService: SharedSendService,
    private authService: AuthService,

  ) {
     if (localStorage.getItem("userType") == "admin")
     {
       this.isAdministrator = true ;
     }
     else {
      this.isAdministrator = false ;
     }


    this.selectedLanguageNOC = localStorage.getItem("currentLanguage");
    this.selectedLanguageCON = localStorage.getItem("currentLanguage");
    this.selectedValueFont = localStorage.getItem("fontsizeValue");
    this.getProfilePicture();

    //const browserLang: string = translate.getBrowserLang();
    //translate.use(browserLang.match(/en|fr|ar/) ? browserLang : "en");


     router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.splitVal = event.url.split('/');
        this.base = this.splitVal[1];
        this.page = this.splitVal[2];
      }
    });
         if (localStorage.getItem("userCurrency") == null){
            console.log('userCurrency  not existe');
            this.defaultCurrencyString = "USD" ;
            localStorage.setItem("userCurrency","USD");
            localStorage.setItem("valueofUSD","1");
         }
         else
         {
          this.defaultCurrencyString =localStorage.getItem("userCurrency") ;
         }

  }
  closeResult = '';

  isEnglish() {
    return this.translate.currentLang != 'ar';
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  ngOnInit(): void {




    if (localStorage.getItem("currentLanguage") !== ""  )
    {
      this.translate.use(localStorage.getItem("currentLanguage"));

    }
  else {
    localStorage.setItem("currentLanguage","en");
  }



     if (Number(localStorage.getItem("fontsize")) > 0){
      this.changeFont(localStorage.getItem("fontsize"));
      this.selectedValueFont = localStorage.getItem("fontsizeValue");
     }
     else
     {
      this.changeFont(18);
      this.selectedValueFont="medium";
     }

    if (localStorage.getItem('userId') && localStorage.getItem('token')) {
      console.log(localStorage.getItem('userId'), localStorage.getItem('token'))
      this.checkToken(localStorage.getItem('userId'), localStorage.getItem('token').toString())

      }

    const counter = interval(1000) ;
  this.counterSubscription =   counter.subscribe(
      (value) => {
        this.secondes = value;
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!');
      }
    );


  }

  /*ngAfterViewInit() {
    this.cdr.detectChanges();
  }*/

  mapGrid() {
    this.router.navigate(['/map-grid']);
  }

  addStyle() {
    if (document.getElementById('submenu').style.display == 'block') {
      document.getElementById('submenu').style.display = 'none';
    } else {
      document.getElementById('submenu').style.display = 'block';
    }
  }

  mentor(name) {
    this.page = name;
    this.router.navigate(['/mentor/dashboard']);
  }

  logout() {


    localStorage.clear();
    this.auth = false;
    this.isPatient = false;
    this.router.navigate(['/login-page']);
  }

  home() {
    this.commonService.nextmessage('main');
    this.router.navigateByUrl('/').then(() => {
      this.router.navigate(['/']);
    });
  }

  navigate(name) {
    this.page = name;
    if (name === 'Admin') {
      this.router.navigate(['/admin']);
      this.commonService.nextmessage('admin');
    }
  }

  ChangeLanguage(language: string) {
    this.translate.use(language);
    this.sharedSendService.sendClickEvent(language);
  }

  CurrentLanguage() {
    return this.translate.currentLang;
  }


  checkToken(id, token) {
    console.log('HcheckToken', id, token )
    this.authService.checkToken(id, token)
      .pipe(first())
      .subscribe(
        data => {
          console.log('HcheckToken', data.token);
          if (data.success) {
            console.log('HcheckToken', data);
            localStorage.setItem('currentUser', data);
            localStorage.setItem('token', data.token);
            localStorage.setItem('userId', data.id);
            localStorage.setItem('isApprouved', data.isApprouved);
            AppComponent.Uname = data.name;
            AppComponent.auth = true;
            this.data = data;


          }
          else {
            console.log('HcheckToken else', null);
            AppComponent.auth = false;
            localStorage.setItem('currentUser', null);
            localStorage.setItem('token', null);
            localStorage.setItem('userId', null);
          }
        }
      )

  }
  userinfo() {
    this.name = AppComponent.Uname;

    return AppComponent.auth
  }
  tutor() {
    this.modalService.dismissAll()
    this.router.navigate(['register']);
  }
  client() {
    this.modalService.dismissAll()
    this.router.navigate(['clientregister']);

  }
  gotodash() {
    console.log('dash')
    if (this.data.type == 'client') {
      this.router.navigate(['/']);
    }
    else if (this.data.type == 'admin') {
      this.router.navigate(['admin/dashboard']);
    }
    else if (this.data.type == 'tutor') {

      if (this.data.isApprouved >6 ) {
        this.router.navigate(['mentor/dashboard']);

      } else {
        this.router.navigate(['blank']);
      }

    }
  }




  ngAfterViewInit() {
    this.loadDynmicallyScript('./../../../assets/admin/js/script.js');
  }
  loadDynmicallyScript(js) {
    var script = document.createElement('script');
    script.src = js;
    script.async = false;
    document.head.appendChild(script);
    script.onload = () => this.doSomethingWhenScriptIsLoaded();
  }

  doSomethingWhenScriptIsLoaded() {}
  change(name) {
    this.page = name;
    this.commonService.nextmessage('admin');
  }


  main() {
    this.commonService.nextmessage('main');
  }
  clickLogout() {
    window.location.href = '/index';
  }
  bell() {
    this.bellCollapsed = !this.bellCollapsed;
    if (!this.userCollapsed) {
      this.userCollapsed = true;
    }
  }
  user() {
    this.userCollapsed = !this.userCollapsed;
    if (!this.bellCollapsed) {
      this.bellCollapsed = true;
    }
  }
















  isMeetingTime(){
    // return true;
  }
  joinMeeting(){


    this.authService.selectbooksclientid(5).subscribe((data) => {

data['rows'].forEach(element => {

  console.log(element['videoCall'].page_size);

});
    });
  }
  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }
  toEnglish(){
console.log('english');
this.ChangeLanguage("en");
localStorage.setItem("currentLanguage","en");
  }
  toArabic(){
    console.log('arabic');
    this.ChangeLanguage("ar");
    localStorage.setItem("currentLanguage","ar");
  }
  toFrensh(){
    console.log('frensh');
    this.ChangeLanguage("fr");
    localStorage.setItem("currentLanguage","fr");
  }
  setCurrency(currency)
  {
    //console.log("currency");
    localStorage.setItem("userCurrency",currency);
    console.log(localStorage.getItem("userCurrency"));
    this.authService.getCurrency(localStorage.getItem("userCurrency")).subscribe(
      data=>{

        localStorage.setItem("valueofUSD",data["USD_"+localStorage.getItem("userCurrency")]);
        console.log(localStorage.getItem("valueofUSD"));
      }
    );
    this.sharedSendService.sendClickEvent("Reload");


  }

  onDarkModeSwitch({ checked } : MatSlideToggleChange){
 this.darkModeSwitched.emit(checked) ;
}
goToAdministration(){
  this.router.navigate(['/admin/dashboard']);
}
getProfilePicture()
{
  this.authService.selectUsers().subscribe(data=>{
   //console.log(data["rows"]) ;
    data["rows"].forEach(element => {
       if (element.id == localStorage.getItem("userId")){
         this.urlPhotoUser = element.photo ;
         console.log(this.urlPhotoUser);
       }
    });

  })
}
goToProfile()
{
if (localStorage.getItem('userType') == "client")
{
  this.router.navigate(['/mentee/dashboard']);
}


}




  CurrencyList: DropDownList[] = [
    {code:"AFN",text:"Afghanistan Afghanis – AFN"},
    {code:"ALL",text:"Albania Leke – ALL"},
    {code:"DZD",text:"Algeria Dinars – DZD"},
    {code:"ARS",text:"Argentina Pesos – ARS"},
    {code:"AUD",text:"Australia Dollars – AUD"},
    {code:"ATS",text:"Austria Schillings – ATS"},
    {code:"BSD",text:"Bahamas Dollars – BSD"},
    {code:"BHD",text:"Bahrain Dinars – BHD"},
    {code:"BDT",text:"Bangladesh Taka – BDT"},
    {code:"BBD",text:"Barbados Dollars – BBD"},
    {code:"BEF",text:"Belgium Francs – BEF"},
    {code:"BMD",text:"Bermuda Dollars – BMD"},
    {code:"BRL",text:"Brazil Reais – BRL"},
    {code:"BGN",text:"Bulgaria Leva – BGN"},
    {code:"CAD",text:"Canada Dollars – CAD"},
    {code:"XOF",text:"CFA BCEAO Francs – XOF"},
    {code:"XAF",text:"CFA BEAC Francs – XAF"},
    {code:"CLP",text:"Chile Pesos – CLP"},
    {code:"CNY",text:"China Yuan Renminbi – CNY"},
    {code:"COP",text:"Colombia Pesos – COP"},
    {code:"XPF",text:"CFP Francs – XPF"},
    {code:"CRC",text:"Costa Rica Colones – CRC"},
    {code:"HRK",text:"Croatia Kuna – HRK"},
    {code:"CYP",text:"Cyprus Pounds – CYP"},
    {code:"CZK",text:"Czech Republic Koruny – CZK"},
    {code:"DKK",text:"Denmark Kroner – DKK"},
    {code:"DEM",text:"Deutsche (Germany) Marks – DEM"},
    {code:"DOP",text:"Dominican Republic Pesos – DOP"},
    {code:"NLG",text:"Dutch (Netherlands) Guilders - NLG"},
    {code:"XCD",text:"Eastern Caribbean Dollars – XCD"},
    {code:"EGP",text:"Egypt Pounds – EGP"},
    {code:"EEK",text:"Estonia Krooni – EEK"},
    {code:"EUR",text:"Euro – EUR"},
    {code:"FJD",text:"Fiji Dollars – FJD"},
    {code:"FIM",text:"Finland Markkaa – FIM"},
    {code:"FRF",text:"France Francs – FRF"},
    {code:"DEM",text:"Germany Deutsche Marks – DEM"},
    {code:"XAU",text:"Gold Ounces – XAU"},
    {code:"GRD",text:"Greece Drachmae – GRD"},
    {code:"GTQ",text:"Guatemalan Quetzal – GTQ"},
    {code:"NLG",text:"Holland (Netherlands) Guilders – NLG"},
    {code:"HKD",text:"Hong Kong Dollars – HKD"},
    {code:"HUF",text:"Hungary Forint – HUF"},
    {code:"ISK",text:"Iceland Kronur – ISK"},
    {code:"XDR",text:"IMF Special Drawing Right – XDR"},
    {code:"INR",text:"India Rupees – INR"},
    {code:"IDR",text:"Indonesia Rupiahs – IDR"},
    {code:"IRR",text:"Iran Rials – IRR"},
    {code:"IQD",text:"Iraq Dinars – IQD"},
    {code:"IEP",text:"Ireland Pounds – IEP"},
    {code:"ILS",text:"Israel New Shekels – ILS"},
    {code:"ITL",text:"Italy Lire – ITL"},
    {code:"JMD",text:"Jamaica Dollars – JMD"},
    {code:"JPY",text:"Japan Yen – JPY"},
    {code:"JOD",text:"Jordan Dinars – JOD"},
    {code:"KES",text:"Kenya Shillings – KES"},
    {code:"KRW",text:"Korea (South) Won – KRW"},
    {code:"KWD",text:"Kuwait Dinars – KWD"},
    {code:"LBP",text:"Lebanon Pounds – LBP"},
    {code:"LUF",text:"Luxembourg Francs – LUF"},
    {code:"MYR",text:"Malaysia Ringgits – MYR"},
    {code:"MTL",text:"Malta Liri – MTL"},
    {code:"MUR",text:"Mauritius Rupees – MUR"},
    {code:"MXN",text:"Mexico Pesos – MXN"},
    {code:"MAD",text:"Morocco Dirhams – MAD"},
    {code:"NLG",text:"Netherlands Guilders – NLG"},
    {code:"NZD",text:"New Zealand Dollars – NZD"},
    {code:"NOK",text:"Norway Kroner – NOK"},
    {code:"OMR",text:"Oman Rials – OMR"},
    {code:"PKR",text:"Pakistan Rupees – PKR"},
    {code:"XPD",text:"Palladium Ounces – XPD"},
    {code:"PEN",text:"Peru Nuevos Soles – PEN"},
    {code:"PHP",text:"Philippines Pesos – PHP"},
    {code:"XPT",text:"Platinum Ounces – XPT"},
    {code:"PLN",text:"Poland Zlotych – PLN"},
    {code:"PTE",text:"Portugal Escudos – PTE"},
    {code:"QAR",text:"Qatar Riyals – QAR"},
    {code:"RON",text:"Romania New Lei – RON"},
    {code:"ROL",text:"Romania Lei – ROL"},
    {code:"RUB",text:"Russia Rubles – RUB"},
    {code:"SAR",text:"Saudi Arabia Riyals – SAR"},
    {code:"XAG",text:"Silver Ounces – XAG"},
    {code:"SGD",text:"Singapore Dollars – SGD"},
    {code:"SKK",text:"Slovakia Koruny – SKK"},
    {code:"SIT",text:"Slovenia Tolars – SIT"},
    {code:"ZAR",text:"South Africa Rand – ZAR"},
    {code:"KRW",text:"South Korea Won – KRW"},
    {code:"ESP",text:"Spain Pesetas – ESP"},
    {code:"XDR",text:"Special Drawing Rights (IMF) – XDR"},
    {code:"LKR",text:"Sri Lanka Rupees – LKR"},
    {code:"SDD",text:"Sudan Dinars – SDD"},
    {code:"SEK",text:"Sweden Kronor – SEK"},
    {code:"CHF",text:"Switzerland Francs – CHF"},
    {code:"TWD",text:"Taiwan New Dollars – TWD"},
    {code:"THB",text:"Thailand Baht – THB"},
    {code:"TTD",text:"Trinidad and Tobago Dollars – TTD"},
    {code:"TND",text:"Tunisia Dinars – TND"},
    {code:"TRY",text:"Turkey New Lira – TRY"},
    {code:"AED",text:"United Arab Emirates Dirhams – AED"},
    {code:"GBP",text:"United Kingdom Pounds – GBP"},
    {code:"USD",text:"United States Dollars – USD"},
    {code:"VEB",text:"Venezuela Bolivares – VEB"},
    {code:"VND",text:"Vietnam Dong – VND"},
    {code:"ZMK",text:"Zambia Kwacha – ZMK"},
    ]

    changeFont(operator) {
 try{
      localStorage.setItem("fontsize",operator);
      if (operator == "12"){localStorage.setItem("fontsizeValue","small");}
      else if (operator == "18"){localStorage.setItem("fontsizeValue","medium");}
      else{localStorage.setItem("fontsizeValue","large");}
      this.fontSize = operator ;
      (this.para.nativeElement as HTMLParagraphElement).style.fontSize = `${this.fontSize}px`;
      console.log("Reload");

    }catch(error){console.error(error);}
    this.sharedSendService.sendClickEvent("Reload");
    }

}

