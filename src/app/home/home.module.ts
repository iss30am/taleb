import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { TooltipModule } from 'ng2-tooltip-directive';
import { TranslateModule } from '@ngx-translate/core';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
    declarations: [HomeComponent],
    imports: [
        NgSelectModule,
        CommonModule,
       
        HomeRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        AutocompleteLibModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        SlickCarouselModule,
        TooltipModule

    ],
    exports: [TranslateModule]
})
export class HomeModule { }
