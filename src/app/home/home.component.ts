import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CommonServiceService } from '../common-service.service';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { SlickCarouselComponent } from 'ngx-slick-carousel';
import { EducationService } from '../services/EducationService';
import { TranslateService } from '@ngx-translate/core';
import { SharedSendService } from '../services/SharedSendService';
import { level } from '../models/level';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

declare const $: any;

export interface Doctors {
  id: number;
  doctor_name: string;
  speciality: string;
  Education: string;
  location: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  // encapsulation : ViewEncapsulation.None
})
export class HomeComponent implements OnInit, OnDestroy {
  @ViewChild('slickModal') slickModal: SlickCarouselComponent;

@ViewChild('para', { static: true }) para: ElementRef;

  specialityList: any = [];
  mentors: any = [];
  slidepage: any;
  positions: any = [];
  employeeCtrl = new FormControl();
  filteredEmployee: Observable<Doctors[]>;
  blogs: any = [];


  isArabic: boolean;
  keyword;
  selectedLanguage: string;

  searchDoctor = [];

  clickEventSubscription: Subscription;
  Category: any;
  Matiers: any;
  Levels: any;
  fontSize: any;

  constructor(
    public router: Router,
    public commonService: CommonServiceService,
    public educationService: EducationService,
    public translate: TranslateService,
    public sharedSendService: SharedSendService
  ) {

 /*if (localStorage.getItem("currentLanguage") === null){
  localStorage.setItem("currentLanguage","en");
  this.translate.use(localStorage.getItem("currentLanguage"));
 }*/


    this.keyword = "levelNameEnglish";
    this.isArabic = false;

    this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {
      if (value === 'ar') { this.keyword = "levelNameArabic"; this.isArabic = true; }
      if (value === 'Reload') { this.ngOnInit();}
    });
  }

  isEnglish() {
    return this.translate.currentLang != 'ar';
  }
  _Category
  _Matiers
  _Levels
  getLevels(): void {
    this.educationService.getLevels().subscribe((data) => {

      this.Levels = data["rows"];
      console.log('levels', this.levels)


    });
  }
  levels(arg0: string, levels: any) {
    throw new Error('Method not implemented.');
  }
  getCategory(): void {
    this.educationService.getCategory().subscribe((data) => {

      this.Category = data["rows"];
      console.log('Category', this.Category)


    });
  }
  getMatiers(): void {
    this.educationService.getMatiers().subscribe((data) => {

      this.Matiers = data["rows"];
      console.log('Matiers', this.Matiers)


    });
  }


  ngOnDestroy() {
    this.clickEventSubscription.unsubscribe();
  }
  ngOnInit() {
    this.changeFont();
    /*if (Number(localStorage.getItem("fontsize")) > 0){
      this.changeFont(localStorage.getItem("fontsize"));
     }
     else
     {
      this.changeFont(18);
     }
     */
    this.filteredEmployee = this.employeeCtrl.valueChanges.pipe(
      startWith(''),
      map((employee) =>
        employee ? this._filterEmployees(employee) : this.mentors.slice()
      )
    );

    this.getLevels();
    this.getCategory();
    this.getMatiers();

    /*
      this. getMatiers();
      window.scrollTo(0, 0);
      this.getspeciality();
      this.getMentors();
      this.getblogs();
      this.getPositions();
    */
    // User's voice slider
    $('.testi-slider').each(function () {
      var $show = $(this).data('show');
      var $arr = $(this).data('arrow');
      var $dots = !$arr;
      var $m_show = $show;
      if ($show == 3) $m_show = $show - 1;
      $(this).slick({
        slidesToShow: $show,
        slidesToScroll: 1,
        arrows: $arr,
        autoplay: false,
        autoplaySpeed: 6000,
        adaptiveHeight: true,
        prevArrow:
          '<button type="button" class="prev-nav"><i class="icon ion-ios-arrow-back"></i></button>',
        nextArrow:
          '<button type="button" class="next-nav"><i class="icon ion-ios-arrow-forward"></i></button>',
        responsive: [
          {
            breakpoint: 991,
            settings: {
              slidesToShow: $m_show,
              slidesToScroll: 1,
              infinite: true,
              arrows: $arr,
              dots: $dots,
            },
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              dots: true,
            },
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              dots: true,
            },
          },
        ],
      });
    });
  }
  private _filterEmployees(value: string): Doctors[] {
    const filterValue = value.toLowerCase();
    return this.mentors.filter(
      (state) => state.doctor_name.toLowerCase().indexOf(filterValue) === 0
    );
  }

  slides = [
    {
      img: 'assets/img/user/user1.jpg',
      profile_name: 'Donna Yancey',
      author: 'Digital Marketer',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user2.jpg',
      profile_name: 'James Amen',
      author: 'UNIX, Calculus, Trigonometry',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user3.jpg',
      profile_name: 'Marvin Downey',
      author: 'ASP.NET,Computer Gaming',
      country: ' Paris, France',
    },
    {
      img: 'assets/img/user/user4.jpg',
      profile_name: 'Betty Hairston',
      author: 'Computer Programming',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user.jpg',
      profile_name: 'Jose Anderson',
      author: 'Digital Marketer',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user6.jpg',
      profile_name: 'Aaron Pietrzak',
      author: 'UNIX, Calculus, Trigonometry',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user7.jpg',
      profile_name: 'Brian Martinez',
      author: 'ASP.NET,Computer Gaming',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user9.jpg',
      profile_name: 'Brian Martinez',
      author: 'UNIX, Calculus, Trigonometry',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user15.jpg',
      profile_name: 'Misty Lundy',
      author: 'Computer Programming',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user13.jpg',
      profile_name: 'Vern Campbell',
      author: 'Digital Marketer',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user12.jpg',
      profile_name: 'Jessica Fogarty',
      author: 'UNIX,Calculus,Trigonometry',
      country: 'Paris, France',
    },
    {
      img: 'assets/img/user/user11.jpg',
      profile_name: 'Evelyn Stafford',
      author: 'ASP.NET,Computer Gaming',
      country: 'Paris, France',
    },
  ];
  slideConfig = {
    dots: true,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  getPositions() {
    this.commonService.getPositions().subscribe((res) => {
      this.positions = res;
    });
  }

  getspeciality() {
    this.commonService.getSpeciality().subscribe((res) => {
      this.specialityList = res;
    });
  }

  windowTop() {
    window.scrollTo(0, 0);
  }

  getMentors() {
    /*
    this.commonService.getMentors().subscribe((res) => {
     this.mentors = res;
     this.slidepage = {
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [
   {
   breakpoint: 1024,
   settings: {
   slidesToShow: 3,
   },
   },
   {
   breakpoint: 600,
   settings: {
   slidesToShow: 2,
   },
   },
   {
   breakpoint: 480,
   settings: {
   slidesToShow: 1,
   },
   },
      ],
     };
     this.levels = [];
     this.mentors.forEach((index, i) => {
      this.levels.push({
   id: index.id,
   name: index.doctor_name,
      });
     });
    });*/
  }

  getblogs() {
    this.commonService.getBlogs().subscribe((res) => {
      this.blogs = res;
    });
  }

  selectEvent(item) {
    // let filter = this.matiers.filter((a) => a.arabicName === item.id);

    this.sharedSendService.sendClickEvent(item.id);


    this.router.navigate(['/search-mentor', { id: 1 }]);
  }
  next() {
    console.log(this._Levels)
    this.router.navigate(['/search-mentor', { level: this._Matiers }]);
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {

  }

  appleStore()
  {
  //  console.log('appleStore');
   window.location.href='https://play.google.com/store/apps/details?id=com.example';
  }
  googlePlay(){
  //  console.log('googlePlay');
  window.location.href='https://play.google.com/store/apps/details?id=com.example';
  }
  changeFont() {
     (this.para.nativeElement as HTMLParagraphElement).style.fontSize = `${Number (localStorage.getItem("fontsize"))}px`;

    }
}
