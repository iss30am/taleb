import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonServiceService } from '../common-service.service';
import { ToastrService } from 'ngx-toastr';
import { EducationService } from '../services/EducationService';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedSendService } from '../services/SharedSendService';

@Component({
    selector: 'app-mentor-profile',
    templateUrl: './mentor-profile.component.html',
    styleUrls: ['./mentor-profile.component.css'],
})
export class MentorProfileComponent implements OnInit {
  valueofUSD : number  = parseFloat(localStorage.getItem("valueofUSD"))  ;
    id;
    doctorDetails;
    tutorid: string;
    tarif: any;
    @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
    selectedtarif: any;
    originaltutor: any;
  userCurrency: string;
  clickEventSubscription: any;
    constructor(private modal: NgbModal,
        public translate: TranslateService,
        public commonService: CommonServiceService, private educationService: EducationService,
        private route: ActivatedRoute, private activatedRoute: ActivatedRoute,
        private toastr: ToastrService,
        public sharedSendService: SharedSendService
    ) {
      this.userCurrency  =localStorage.getItem("userCurrency");
        this.activatedRoute.queryParams.subscribe(params => {
            this.tutorid = this.route.snapshot.paramMap.get("id")
            console.log('activatedRoute', this.tutorid)
            this.educationService.selectProfessorsProfilesid(this.tutorid).subscribe(async (tutor) => {
                this.tutor = tutor.rows[0]
                console.log('activatedRoute', this.tutor)
                this.selecteddate = 0;
                this.educationService.fulltarif(this.tutorid).subscribe(async (tarif) => {
                    console.log('tarif', tarif)
                    this.tarif = tarif. rows
                })

                console.log(this.tutor)
                this.tutor.urlvideo = this.youtube(this.tutor.urlvideo)
                var order = 0
                this.firstday = (new Date()).getDay()
                for (let index = this.firstday; index <= 6; index++) {
                    this.week.push({ index: index, day: this.days[index], day_a: this.days_a[index], order: order })
                    order++;
                }

                for (let index = 0; index < this.firstday; index++) {
                    this.week.push({ index: index, day: this.days[index], day_a: this.days_a[index], order: order })
                    order++;
                }
                const currentYear = new Date().getFullYear();



                // this.firstday = (new Date()).getDay()
                // for (let index = this.firstday; index <= 6; index++) {
                //     this.week.push({ index: index, day: this.days[index], day_a: this.days_a[index] })
                // }

                // for (let index = 0; index < this.firstday; index++) {
                //     this.week.push({ index: index, day: this.days[index], day_a: this.days_a[index] })
                // }
                console.log(this.week)
                var date = new Date();
                var date2 = new Date();
                var date3 = new Date();
                var date4 = new Date();
                var date5 = new Date();
                date.setHours(0)
                date.setMinutes(1)
                date2.setHours(0);
                date2.setMinutes(1);
                date2.setDate(date2.getDate() + 7);
                date3.setHours(0);
                date3.setMinutes(1);
                date3.setDate(date3.getDate() + 14);
                date4.setHours(0);
                date4.setMinutes(1);
                date4.setDate(date4.getDate() + 21);
                date5.setHours(0);
                date5.setMinutes(1);
                date5.setDate(date5.getDate() + 28);

                this.dates = [
                    { timestart: date.getTime(), timeend: date2.getTime(), day: this.days[date.getDay()], day_a: this.days_a[date.getDay()], month: this.month[date.getMonth()], month_ar: this.month_ar[date.getMonth()], date: date, datee: date2 },
                    { timestart: date2.getTime(), timeend: date3.getTime(), day: this.days[date2.getDay()], day_a: this.days_a[date2.getDay()], month: this.month[date2.getMonth()], month_ar: this.month_ar[date2.getMonth()], date: date2, datee: date3 },
                    { timestart: date3.getTime(), timeend: date4.getTime(), day: this.days[date3.getDay()], day_a: this.days_a[date3.getDay()], month: this.month[date3.getMonth()], month_ar: this.month_ar[date3.getMonth()], date: date3, datee: date4 },
                    { timestart: date4.getTime(), timeend: date5.getTime(), day: this.days[date4.getDay()], day_a: this.days_a[date4.getDay()], month: this.month[date4.getMonth()], month_ar: this.month_ar[date4.getMonth()], date: date4, datee: date5 }

                ]
                this.getBasket();
                this.weekcalendar(this.selecteddate)

            })
            // Print the parameter to the console.



        });

        this.clickEventSubscription = this.sharedSendService.getEvent().subscribe(value => {

          if (value === 'Reload') { this.ngOnInit();}
        });

    }
    images = [
        {
            path: 'assets/img/features/feature-01.jpg',
        },
        {
            path: 'assets/img/features/feature-02.jpg',
        },
        {
            path: 'assets/img/features/feature-03.jpg',
        },
        {
            path: 'assets/img/features/feature-04.jpg',
        },
    ];
    ngOnInit(): void {
        window.scrollTo(0, 0);
        this.id = this.route.snapshot.queryParams['id'];
        this.getDoctorsDetails();
    }

    getDoctorsDetails() {
        if (!this.id) {
            this.id = 1;
        }
        this.commonService.getDoctorDetails(this.id).subscribe((res) => {
            this.doctorDetails = res;
        });
    }

    addFav() {
        this.commonService.createFav(this.doctorDetails).subscribe((res) => {
            this.toastr.success('', 'Added to favourite successfully!');
            document.getElementById('fav-btn').style.background = '#fb1612';
            document.getElementById('fav-btn').style.color = '#fff';
        });
    }






    Levels: any;

    Level: any;
    Matiers: any;
    Category: any;
    tutors: any;
    segment: any;
    firstday: any;
    week = [];
    days = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    days_a = ['الأحَد', 'إثنين', 'ثلاثاء', 'أربعاء', 'خميس', 'جمعة', 'سبت'];
    month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    month_ar = ["يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
    dates: any;
    selecteddate = 0
    selectedtext: any;
    tutor: any;
    basket : any;
    selected_date

    selected_date2
    emptyday = { h0: 0, h1: 0, h2: 0, h3: 0, h4: 0, h5: 0, h6: 0, h7: 0, h8: 0, h9: 0, h10: 0, h11: 0, h12: 0, h13: 0, h14: 0, h15: 0, h16: 0, h17: 0, h18: 0, h19: 0, h20: 0, h21: 0, h22: 0, h23: 0 }
    weekcalendar(n) {
        var D = new Date(this.dates[n].date)
        var E = new Date(this.dates[n].datee)
        this.selectedtext = { ar: D.getDate() + '' + this.month_ar[D.getMonth()] + ' - ' + E.getDate() + '' + this.month_ar[E.getMonth()], en: D.getDate() + '' + this.month[D.getMonth()] + ' - ' + E.getDate() + '' + this.month[E.getMonth()] }

        this.selected_date = D;
        this.selected_date2 = E;

        this.educationService.selectbooksDateDebOfBook(D, E)
            .subscribe(async (res) => {
                var books = res.rows;
                var tutor = this.tutor
                tutor.week = [];
                var week = JSON.parse(JSON.stringify(this.week));
                week.forEach(day => {
                    var emptyday = JSON.parse(JSON.stringify(this.emptyday));
                    if (day.index == 6) {
                        emptyday.h0 = this.checkBook(tutor, books, tutor.d7_h0, 'd7_h0')
                        emptyday.h1 = this.checkBook(tutor, books, tutor.d7_h1, 'd7_h1')
                        emptyday.h2 = this.checkBook(tutor, books, tutor.d7_h2, 'd7_h2')
                        emptyday.h3 = this.checkBook(tutor, books, tutor.d7_h3, 'd7_h3')
                        emptyday.h4 = this.checkBook(tutor, books, tutor.d7_h4, 'd7_h4')
                        emptyday.h5 = this.checkBook(tutor, books, tutor.d7_h5, 'd7_h5')
                        emptyday.h6 = this.checkBook(tutor, books, tutor.d7_h6, 'd7_h6')
                        emptyday.h7 = this.checkBook(tutor, books, tutor.d7_h7, 'd7_h7')
                        emptyday.h8 = this.checkBook(tutor, books, tutor.d7_h8, 'd7_h8')
                        emptyday.h9 = this.checkBook(tutor, books, tutor.d7_h9, 'd7_h9')

                        emptyday.h10 = this.checkBook(tutor, books, tutor.d7_h10, 'd7_h10')
                        emptyday.h11 = this.checkBook(tutor, books, tutor.d7_h11, 'd7_h11')
                        emptyday.h12 = this.checkBook(tutor, books, tutor.d7_h12, 'd7_h12')
                        emptyday.h13 = this.checkBook(tutor, books, tutor.d7_h13, 'd7_h13')
                        emptyday.h14 = this.checkBook(tutor, books, tutor.d7_h14, 'd7_h14')
                        emptyday.h15 = this.checkBook(tutor, books, tutor.d7_h15, 'd7_h15')
                        emptyday.h16 = this.checkBook(tutor, books, tutor.d7_h16, 'd7_h16')
                        emptyday.h17 = this.checkBook(tutor, books, tutor.d7_h17, 'd7_h17')
                        emptyday.h18 = this.checkBook(tutor, books, tutor.d7_h18, 'd7_h18')
                        emptyday.h19 = this.checkBook(tutor, books, tutor.d7_h19, 'd7_h19')

                        emptyday.h20 = this.checkBook(tutor, books, tutor.d7_h20, 'd7_h20')
                        emptyday.h21 = this.checkBook(tutor, books, tutor.d7_h21, 'd7_h21')
                        emptyday.h22 = this.checkBook(tutor, books, tutor.d7_h22, 'd7_h22')
                        emptyday.h23 = this.checkBook(tutor, books, tutor.d7_h23, 'd7_h23')





                    }

                    if (day.index == 0) {
                        emptyday.h0 = this.checkBook(tutor, books, tutor.d1_h0, 'd1_h0')
                        emptyday.h1 = this.checkBook(tutor, books, tutor.d1_h1, 'd1_h1')
                        emptyday.h2 = this.checkBook(tutor, books, tutor.d1_h2, 'd1_h2')
                        emptyday.h3 = this.checkBook(tutor, books, tutor.d1_h3, 'd1_h3')
                        emptyday.h4 = this.checkBook(tutor, books, tutor.d1_h4, 'd1_h4')
                        emptyday.h5 = this.checkBook(tutor, books, tutor.d1_h5, 'd1_h5')
                        emptyday.h6 = this.checkBook(tutor, books, tutor.d1_h6, 'd1_h6')
                        emptyday.h7 = this.checkBook(tutor, books, tutor.d1_h7, 'd1_h7')
                        emptyday.h8 = this.checkBook(tutor, books, tutor.d1_h8, 'd1_h8')
                        emptyday.h9 = this.checkBook(tutor, books, tutor.d1_h9, 'd1_h9')

                        emptyday.h10 = this.checkBook(tutor, books, tutor.d1_h10, 'd1_h10')
                        emptyday.h11 = this.checkBook(tutor, books, tutor.d1_h11, 'd1_h11')
                        emptyday.h12 = this.checkBook(tutor, books, tutor.d1_h12, 'd1_h12')
                        emptyday.h13 = this.checkBook(tutor, books, tutor.d1_h13, 'd1_h13')
                        emptyday.h14 = this.checkBook(tutor, books, tutor.d1_h14, 'd1_h14')
                        emptyday.h15 = this.checkBook(tutor, books, tutor.d1_h15, 'd1_h15')
                        emptyday.h16 = this.checkBook(tutor, books, tutor.d1_h16, 'd1_h16')
                        emptyday.h17 = this.checkBook(tutor, books, tutor.d1_h17, 'd1_h17')
                        emptyday.h18 = this.checkBook(tutor, books, tutor.d1_h18, 'd1_h18')
                        emptyday.h19 = this.checkBook(tutor, books, tutor.d1_h19, 'd1_h19')

                        emptyday.h20 = this.checkBook(tutor, books, tutor.d1_h20, 'd1_h20')
                        emptyday.h21 = this.checkBook(tutor, books, tutor.d1_h21, 'd1_h21')
                        emptyday.h22 = this.checkBook(tutor, books, tutor.d1_h22, 'd1_h22')
                        emptyday.h23 = this.checkBook(tutor, books, tutor.d1_h23, 'd1_h23')
                    }


                    if (day.index == 1) {
                        emptyday.h0 = this.checkBook(tutor, books, tutor.d2_h0, 'd2_h0')
                        emptyday.h1 = this.checkBook(tutor, books, tutor.d2_h1, 'd2_h1')
                        emptyday.h2 = this.checkBook(tutor, books, tutor.d2_h2, 'd2_h2')
                        emptyday.h3 = this.checkBook(tutor, books, tutor.d2_h3, 'd2_h3')
                        emptyday.h4 = this.checkBook(tutor, books, tutor.d2_h4, 'd2_h4')
                        emptyday.h5 = this.checkBook(tutor, books, tutor.d2_h5, 'd2_h5')
                        emptyday.h6 = this.checkBook(tutor, books, tutor.d2_h6, 'd2_h6')
                        emptyday.h7 = this.checkBook(tutor, books, tutor.d2_h7, 'd2_h7')
                        emptyday.h8 = this.checkBook(tutor, books, tutor.d2_h8, 'd2_h8')
                        emptyday.h9 = this.checkBook(tutor, books, tutor.d2_h9, 'd2_h9')

                        emptyday.h10 = this.checkBook(tutor, books, tutor.d2_h10, 'd2_h10')
                        emptyday.h11 = this.checkBook(tutor, books, tutor.d2_h11, 'd2_h11')
                        emptyday.h12 = this.checkBook(tutor, books, tutor.d2_h12, 'd2_h12')
                        emptyday.h13 = this.checkBook(tutor, books, tutor.d2_h13, 'd2_h13')
                        emptyday.h14 = this.checkBook(tutor, books, tutor.d2_h14, 'd2_h14')
                        emptyday.h15 = this.checkBook(tutor, books, tutor.d2_h15, 'd2_h15')
                        emptyday.h16 = this.checkBook(tutor, books, tutor.d2_h16, 'd2_h16')
                        emptyday.h17 = this.checkBook(tutor, books, tutor.d2_h17, 'd2_h17')
                        emptyday.h18 = this.checkBook(tutor, books, tutor.d2_h18, 'd2_h18')
                        emptyday.h19 = this.checkBook(tutor, books, tutor.d2_h19, 'd2_h19')

                        emptyday.h20 = this.checkBook(tutor, books, tutor.d2_h20, 'd2_h20')
                        emptyday.h21 = this.checkBook(tutor, books, tutor.d2_h21, 'd2_h21')
                        emptyday.h22 = this.checkBook(tutor, books, tutor.d2_h22, 'd2_h22')
                        emptyday.h23 = this.checkBook(tutor, books, tutor.d2_h23, 'd2_h23')
                    }


                    if (day.index == 2) {
                        emptyday.h0 = this.checkBook(tutor, books, tutor.d3_h0, 'd3_h0')
                        emptyday.h1 = this.checkBook(tutor, books, tutor.d3_h1, 'd3_h1')
                        emptyday.h2 = this.checkBook(tutor, books, tutor.d3_h2, 'd3_h2')
                        emptyday.h3 = this.checkBook(tutor, books, tutor.d3_h3, 'd3_h3')
                        emptyday.h4 = this.checkBook(tutor, books, tutor.d3_h4, 'd3_h4')
                        emptyday.h5 = this.checkBook(tutor, books, tutor.d3_h5, 'd3_h5')
                        emptyday.h6 = this.checkBook(tutor, books, tutor.d3_h6, 'd3_h6')
                        emptyday.h7 = this.checkBook(tutor, books, tutor.d3_h7, 'd3_h7')
                        emptyday.h8 = this.checkBook(tutor, books, tutor.d3_h8, 'd3_h8')
                        emptyday.h9 = this.checkBook(tutor, books, tutor.d3_h9, 'd3_h9')

                        emptyday.h10 = this.checkBook(tutor, books, tutor.d3_h10, 'd3_h10')
                        emptyday.h11 = this.checkBook(tutor, books, tutor.d3_h11, 'd3_h11')
                        emptyday.h12 = this.checkBook(tutor, books, tutor.d3_h12, 'd3_h12')
                        emptyday.h13 = this.checkBook(tutor, books, tutor.d3_h13, 'd3_h13')
                        emptyday.h14 = this.checkBook(tutor, books, tutor.d3_h14, 'd3_h14')
                        emptyday.h15 = this.checkBook(tutor, books, tutor.d3_h15, 'd3_h15')
                        emptyday.h16 = this.checkBook(tutor, books, tutor.d3_h16, 'd3_h16')
                        emptyday.h17 = this.checkBook(tutor, books, tutor.d3_h17, 'd3_h17')
                        emptyday.h18 = this.checkBook(tutor, books, tutor.d3_h18, 'd3_h18')
                        emptyday.h19 = this.checkBook(tutor, books, tutor.d3_h19, 'd3_h19')

                        emptyday.h20 = this.checkBook(tutor, books, tutor.d3_h20, 'd3_h20')
                        emptyday.h21 = this.checkBook(tutor, books, tutor.d3_h21, 'd3_h21')
                        emptyday.h22 = this.checkBook(tutor, books, tutor.d3_h22, 'd3_h22')
                        emptyday.h23 = this.checkBook(tutor, books, tutor.d3_h23, 'd3_h23')
                    }


                    if (day.index == 3) {
                        emptyday.h0 = this.checkBook(tutor, books, tutor.d4_h0, 'd4_h0')
                        emptyday.h1 = this.checkBook(tutor, books, tutor.d4_h1, 'd4_h1')
                        emptyday.h2 = this.checkBook(tutor, books, tutor.d4_h2, 'd4_h2')
                        emptyday.h3 = this.checkBook(tutor, books, tutor.d4_h3, 'd4_h3')
                        emptyday.h4 = this.checkBook(tutor, books, tutor.d4_h4, 'd4_h4')
                        emptyday.h5 = this.checkBook(tutor, books, tutor.d4_h5, 'd4_h5')
                        emptyday.h6 = this.checkBook(tutor, books, tutor.d4_h6, 'd4_h6')
                        emptyday.h7 = this.checkBook(tutor, books, tutor.d4_h7, 'd4_h7')
                        emptyday.h8 = this.checkBook(tutor, books, tutor.d4_h8, 'd4_h8')
                        emptyday.h9 = this.checkBook(tutor, books, tutor.d4_h9, 'd4_h9')

                        emptyday.h10 = this.checkBook(tutor, books, tutor.d4_h10, 'd4_h10')
                        emptyday.h11 = this.checkBook(tutor, books, tutor.d4_h11, 'd4_h11')
                        emptyday.h12 = this.checkBook(tutor, books, tutor.d4_h12, 'd4_h12')
                        emptyday.h13 = this.checkBook(tutor, books, tutor.d4_h13, 'd4_h13')
                        emptyday.h14 = this.checkBook(tutor, books, tutor.d4_h14, 'd4_h14')
                        emptyday.h15 = this.checkBook(tutor, books, tutor.d4_h15, 'd4_h15')
                        emptyday.h16 = this.checkBook(tutor, books, tutor.d4_h16, 'd4_h16')
                        emptyday.h17 = this.checkBook(tutor, books, tutor.d4_h17, 'd4_h17')
                        emptyday.h18 = this.checkBook(tutor, books, tutor.d4_h18, 'd4_h18')
                        emptyday.h19 = this.checkBook(tutor, books, tutor.d4_h19, 'd4_h19')

                        emptyday.h20 = this.checkBook(tutor, books, tutor.d4_h20, 'd4_h20')
                        emptyday.h21 = this.checkBook(tutor, books, tutor.d4_h21, 'd4_h21')
                        emptyday.h22 = this.checkBook(tutor, books, tutor.d4_h22, 'd4_h22')
                        emptyday.h23 = this.checkBook(tutor, books, tutor.d4_h23, 'd4_h23')

                    }


                    if (day.index == 4) {
                        emptyday.h0 = this.checkBook(tutor, books, tutor.d5_h0, 'd5_h0')
                        emptyday.h1 = this.checkBook(tutor, books, tutor.d5_h1, 'd5_h1')
                        emptyday.h2 = this.checkBook(tutor, books, tutor.d5_h2, 'd5_h2')
                        emptyday.h3 = this.checkBook(tutor, books, tutor.d5_h3, 'd5_h3')
                        emptyday.h4 = this.checkBook(tutor, books, tutor.d5_h4, 'd5_h4')
                        emptyday.h5 = this.checkBook(tutor, books, tutor.d5_h5, 'd5_h5')
                        emptyday.h6 = this.checkBook(tutor, books, tutor.d5_h6, 'd5_h6')
                        emptyday.h7 = this.checkBook(tutor, books, tutor.d5_h7, 'd5_h7')
                        emptyday.h8 = this.checkBook(tutor, books, tutor.d5_h8, 'd5_h8')
                        emptyday.h9 = this.checkBook(tutor, books, tutor.d5_h9, 'd5_h9')

                        emptyday.h10 = this.checkBook(tutor, books, tutor.d5_h10, 'd5_h10')
                        emptyday.h11 = this.checkBook(tutor, books, tutor.d5_h11, 'd5_h11')
                        emptyday.h12 = this.checkBook(tutor, books, tutor.d5_h12, 'd5_h12')
                        emptyday.h13 = this.checkBook(tutor, books, tutor.d5_h13, 'd5_h13')
                        emptyday.h14 = this.checkBook(tutor, books, tutor.d5_h14, 'd5_h14')
                        emptyday.h15 = this.checkBook(tutor, books, tutor.d5_h15, 'd5_h15')
                        emptyday.h16 = this.checkBook(tutor, books, tutor.d5_h16, 'd5_h16')
                        emptyday.h17 = this.checkBook(tutor, books, tutor.d5_h17, 'd5_h17')
                        emptyday.h18 = this.checkBook(tutor, books, tutor.d5_h18, 'd5_h18')
                        emptyday.h19 = this.checkBook(tutor, books, tutor.d5_h19, 'd5_h19')

                        emptyday.h20 = this.checkBook(tutor, books, tutor.d5_h20, 'd5_h20')
                        emptyday.h21 = this.checkBook(tutor, books, tutor.d5_h21, 'd5_h21')
                        emptyday.h22 = this.checkBook(tutor, books, tutor.d5_h22, 'd5_h22')
                        emptyday.h23 = this.checkBook(tutor, books, tutor.d5_h23, 'd5_h23')
                    }


                    if (day.index == 5) {
                        emptyday.h0 = this.checkBook(tutor, books, tutor.d6_h0, 'd6_h0')
                        emptyday.h1 = this.checkBook(tutor, books, tutor.d6_h1, 'd6_h1')
                        emptyday.h2 = this.checkBook(tutor, books, tutor.d6_h2, 'd6_h2')
                        emptyday.h3 = this.checkBook(tutor, books, tutor.d6_h3, 'd6_h3')
                        emptyday.h4 = this.checkBook(tutor, books, tutor.d6_h4, 'd6_h4')
                        emptyday.h5 = this.checkBook(tutor, books, tutor.d6_h5, 'd6_h5')
                        emptyday.h6 = this.checkBook(tutor, books, tutor.d6_h6, 'd6_h6')
                        emptyday.h7 = this.checkBook(tutor, books, tutor.d6_h7, 'd6_h7')
                        emptyday.h8 = this.checkBook(tutor, books, tutor.d6_h8, 'd6_h8')
                        emptyday.h9 = this.checkBook(tutor, books, tutor.d6_h9, 'd6_h9')

                        emptyday.h10 = this.checkBook(tutor, books, tutor.d6_h10, 'd6_h10')
                        emptyday.h11 = this.checkBook(tutor, books, tutor.d6_h11, 'd6_h11')
                        emptyday.h12 = this.checkBook(tutor, books, tutor.d6_h12, 'd6_h12')
                        emptyday.h13 = this.checkBook(tutor, books, tutor.d6_h13, 'd6_h13')
                        emptyday.h14 = this.checkBook(tutor, books, tutor.d6_h14, 'd6_h14')
                        emptyday.h15 = this.checkBook(tutor, books, tutor.d6_h15, 'd6_h15')
                        emptyday.h16 = this.checkBook(tutor, books, tutor.d6_h16, 'd6_h16')
                        emptyday.h17 = this.checkBook(tutor, books, tutor.d6_h17, 'd6_h17')
                        emptyday.h18 = this.checkBook(tutor, books, tutor.d6_h18, 'd6_h18')
                        emptyday.h19 = this.checkBook(tutor, books, tutor.d6_h19, 'd6_h19')

                        emptyday.h20 = this.checkBook(tutor, books, tutor.d6_h20, 'd6_h20')
                        emptyday.h21 = this.checkBook(tutor, books, tutor.d6_h21, 'd6_h21')
                        emptyday.h22 = this.checkBook(tutor, books, tutor.d6_h22, 'd6_h22')
                        emptyday.h23 = this.checkBook(tutor, books, tutor.d6_h23, 'd6_h23')


                    }
                    tutor.week.push({ today: emptyday, day: day })

                });
                this.tutor = tutor
                this.originaltutor = JSON.parse(JSON.stringify(this.tutor))

                console.log(tutor)




            })


    }
    checkBook(tutor, books, cd, d) {
        var rslt = { o: 0, t: 'E', dh1: d, s: '' }
        if (cd) {
            rslt = { o: 1, t: 'O', dh1: d, s: '' };
            books.forEach(book => { if ((book.dh1 == d || book.dh2 == d) && (tutor.professorsId == book.professorsId)) { rslt = { o: 2, t: "X", dh1: d, s: '' }; } })
        }



        if (rslt.t == 'O') {
            var refBook = this.selecteddate + '_' + d
            if (this.isInBasket(refBook) >= 0) {
                rslt.s = 'selecteddate'
            }
        }


        return rslt;

    }

    nextdate() {
        console.log(this.selecteddate, 'nextdate')
        if (this.selecteddate < 3) {
            this.tutor.week = []
            this.selecteddate = this.selecteddate + 1;
            this.weekcalendar(this.selecteddate)
        }
    }
    backdate() {
        console.log(this.selecteddate, 'selecteddate')
        if (this.selecteddate > 0) {
            this.tutor.week = []
            this.selecteddate = this.selecteddate - 1;
            this.weekcalendar(this.selecteddate)
        }
    }
    isEnglish() {
        return this.translate.currentLang != 'ar';
    }

    getBasket() {
        // MyApp.basket = [];
        this.basket = [];
        var basket=  localStorage.getItem('basket')



            console.log('basket', basket)
        if (localStorage.getItem('basket')) {
                if (basket.length) {
                    // MyApp.basket = basket;

                    this.basket = basket;
                } else {
                    // MyApp.basket = [];
                    this.basket = [];
                }
            } else {
                // MyApp.basket = [];
                this.basket = [];

            }
            console.log('basket', basket)

    }
    setBasket(basket) {
        // MyApp.basket = basket;
     localStorage.setItem('basket', basket);
    }
    isInBasket(refBook) {
        var rsl = -1;
        if (this.basket) {


            for (var i = 0; i < this.basket.length; i++) {

                if (this.basket[i].refBook == refBook) {


                    rsl = i;
                    return i;

                }
            }

        }

        return rsl;
    }
    addtobasket(selected, day) {



        if (selected.t == 'O') {
            var time = selected.dh1.split("h");
            var refBook = this.selecteddate + '_' + selected.dh1
            var time = selected.dh1.split("h");


            if (this.isInBasket(refBook) >= 0) {
                this.basket.splice(this.isInBasket(refBook), 1);

                selected.s = ''

                console.log('splice', refBook)
                this.setBasket(this.basket)
            } else {

                console.log(selected, day,)
                var DateDebOfBook = new Date();





                var selectedhour = Number(time[1])




                DateDebOfBook.setDate((DateDebOfBook.getDate() + (Number(this.selecteddate) * 7) + (Number(day.day.order))))
                DateDebOfBook.setHours(selectedhour)
                DateDebOfBook.setMinutes(3)
                DateDebOfBook.setSeconds(0)
                DateDebOfBook.setMilliseconds(0)
                console.log(DateDebOfBook.toDateString(), Number(this.selecteddate), (Number(this.selecteddate) * 7), Number(day.day.order), selectedhour, (time[1]))

                if (DateDebOfBook > new Date()) {
                    selected.s = 'selecteddate'
                    this.basket.push({ tutor: this.tutor, DateDeb: DateDebOfBook, professorsId: this.tutor.professorsId, refBook: refBook, clientId: 0, DateDebOfBook: DateDebOfBook.getTime(), numberOfHours: 1, levelId: this.selectedtarif.id, statusOfBook: 'Pending', Transaction: '', videoCall: '', paymentStatus: '', paymentWallet: 0, Price: this.selectedtarif.price, paymentOnline: this.selectedtarif.price, dh1: selected.dh1, dh2: '', CreationDate: (new Date().getTime()), _Delete: 0 })
                    this.setBasket(this.basket)
                    console.log('setBasket', refBook)
                } else {
                    if (this.isEnglish()) {
                        alert('You cannot choose this Date')
                    } else {
                        alert('لا يمكنك اختيار هذا التاريخ')
                    }

                }
            }



        }
    }



    ionViewDidEnter() {

        this.getBasket()

    }
    youtube(str) {
try {
    var res = str.split("=");
    return '    <iframe width="560" height = "315" src = "https://www.youtube.com/embed/' + res[1] + ' | safe" frameborder = "0" allow = "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen > </iframe>';

} catch (error) {
    return ''
}
         }

    authuser() {
        return true
       //raja3 true keno il user log in
    }

    login() {

       // hezo lpage login
    }
    checkout() {
        // hezo lpage login
    }
    handleEvent(tarif: string): void {
        // this.modalData = { event, action };


        this.tutor = JSON.parse(JSON.stringify(this.originaltutor))


        this.selectedtarif = tarif
        this.modal.open(this.modalContent, { size: 'lg' });
    }
    basketBtn() {
        if (!this.authuser()) {
            this.login()
        } else {
            this.checkout()
        }

    }
}
